/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include <map>

//#include "../Controller/MenuController.h"
//#include "../Model/DataModel.h"
//#include "../Controller/GameController.h"
//#include "../Controller/SaveController.h"
//#include "../Controller/LoadController.h"

#include "../Input/BaseInput.h"

#include "../View/BaseView.h"
//#include "../View/MenuView.h"
//#include "../View/GameView.h"
//#include "../View/SaveView.h"
//#include "../View/LoadView.h"

#include "../Commands/AppCmds/BaseAppCmd.h"
#include "../Controller/BaseController.h"

#include "./AppData.h"
#include "../Model/MenuModel.h"

enum ControllerEnum {
	MENU,
	GAME
};

class BaseController;

class App {
private:

	//bool for main loop to end running of app
	bool running;
	std::string saveFileName;

	//store the mvc sets for the app
	std::map<ControllerEnum, std::unique_ptr<BaseController>> MVCSets;

	//main data file for the app that can be saved/loaded
	//ref will be passed to controllers for loading/saving stuff
	std::unique_ptr<AppData> appData;

	//current controller being run
	BaseController* controller;


	BaseController* getController (ControllerEnum inControllerEnum);
	void loadAppData (std::string inFileName);
	void initMVC ();
	void initApp ();
	void saveAppData ();
	void handleAppCmd (std::unique_ptr<BaseAppCmd> inAppCmd);
	void mainLoop ();
	std::unique_ptr<AppData> getEmptyAppData ();

protected:
public:

	App (std::string inFileName):
		saveFileName(inFileName) {
//		Debug::info("App constructed.\n");
		try {
			loadAppData(inFileName);
			initMVC();
			initApp();
			mainLoop();
		} catch (std::runtime_error err) {
			std::cout << "Error: " << err.what() << "\n";
			Debug::error(err.what());
			return;
		}
	};

	~App () {
//		Debug::info("App deconstructed.\n");
	};

	void quit ();
	void saveAndQuit ();
	void loadMenu (MenuState& inMenuState);
	void saveGame (std::unique_ptr<Game> inGame);
	void loadGame (std::unique_ptr<Game> inGame);
};
