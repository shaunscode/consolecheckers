/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>

#include "../Game/Game.h"

class Player;
class Game;
class AppData;

using byte = char;

const int MAX_NUM_SAVES = 50;

class FileIO {

private:
	
	//read methods
	std::unique_ptr<GameBoard> readGameBoard (std::ifstream* inInputFile);
	bool readBool (std::ifstream* inInputFile);
	int readInt (std::ifstream* inInputFile);
	long int readLongInt (std::ifstream* inInputFile);
	std::string readString (std::ifstream* inInputFile, int inLen);
	std::unique_ptr<Player> readPlayer (std::ifstream* inInputFile);
	std::unique_ptr<Game> readGame (std::ifstream* inInputFile);

	std::unique_ptr<std::vector<std::unique_ptr<Player>>> readPlayers (std::ifstream* inInputFile);
	std::unique_ptr<std::vector<std::unique_ptr<Game>>> readGames (std::ifstream* inInputFile);
	
	//write methods
	void writeGameBoard (std::ofstream* inOutputFile, GameBoard* inGameBoard);
	void writeBool (std::ofstream* inOutputFile, bool inBool);
	void writeInt (std::ofstream* inOutputFile, int inInt);
	void writeLongInt (std::ofstream* inOutputFile, long int inInt);
	void writeString (std::ofstream* inOutputFile, std::string* inString);
	void writePlayer (std::ofstream* inOutputFile, Player* inPlayer);
	void writeGame (std::ofstream* inOutputFile, Game* inGame);

	void writePlayers (std::ofstream* inOutputFile, std::vector<std::unique_ptr<Player>>* inPlayers);
	void writeGames (std::ofstream* inOutputFile, std::vector<std::unique_ptr<Game>>* inGames);

	//stream getters based on file name
	std::unique_ptr<std::ofstream> getOutputStream  (std::string inFileName);
	std::unique_ptr<std::ifstream> getInputStream (std::string inFileName);
	
protected:
public:

	std::unique_ptr<AppData> loadAppData (std::string inFileName);
	void saveAppData (std::string inFileName, AppData* inAppData);
};
