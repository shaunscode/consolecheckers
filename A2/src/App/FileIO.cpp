
	 /*	data format in teh following order
	 *
	 *	int - num players
	 *	{
	 *		int - player name lenght
	 *		char* - players name
	 *		int - players id
	 *	}
	 *	int - num saved games
	 *	{
	 *		int[64] - cell contents
	 *		int - red player id
	 *		int - red player score
	 *		int - white player id
	 *		int - white player score
	 *	}
	 */
//	reading AppData
//
//	- create an AppData object
//	- get num players stored
//		=> read int
//	- loop that many times to get all players
//		- read player name length
//			=> read int
//		- read players name
//			=> read char block lenght of above
//		- get players id
//			=> read int
//		- create new player with details
//		- add to AppData
//	- get num saved games
//		=> read int
//	- for each game
//		- read 64 ints of
//			=> read char* block 64 long, convert to int's
//		- get player 1 id
//			=> read int
//		- get player 1 score
//			=> read int
//		- get player 2
//			=> read int
//		- get player 2 score
//			=> read int
//		- create Game with details
//		- add to AppData obj
#include "../Debug.h"

#include "./AppData.h"
#include "./IO.h"
#include "./FileIO.h"
#include "../Game/Game.h"
#include "../Game/Player.h"



	//creates and returns an ifstream obj from the filename passed in
	std::unique_ptr<std::ifstream> FileIO::getInputStream  (std::string inFileName) {
		std::unique_ptr<std::ifstream> inInputFile = std::make_unique<std::ifstream>();
		inInputFile->open(inFileName, std::ios::in | std::ios::binary);
		return std::move(inInputFile);
	}

	//creates and returns an ofstream of the file passed in.
	std::unique_ptr<std::ofstream> FileIO::getOutputStream (std::string inFileName) {
		std::unique_ptr<std::ofstream> inOutputFile = std::make_unique<std::ofstream>();
		inOutputFile->open(inFileName, std::ios::out | std::ios::binary | std::ios::trunc);
		return std::move(inOutputFile);
	}

	std::unique_ptr<GameBoard> FileIO::readGameBoard (std::ifstream* inInputFile) {
		std::unique_ptr<GameBoard> board = std::make_unique<GameBoard>();
		for (int i = 0; i < GAME_BOARD_SIZE; i++) {
			for (int j = 0; j < GAME_BOARD_SIZE; j++) {
				(*board)[i][j] = readInt(inInputFile);
			}
		}
		return std::move(board);
	}

	void FileIO::writeGameBoard (std::ofstream* inOutputFile, GameBoard* inGameBoard) {
		for (int i = 0; i < GAME_BOARD_SIZE; i++) {
			for (int j = 0; j < GAME_BOARD_SIZE; j++) {
				writeInt(inOutputFile, (*inGameBoard)[i][j]);
			}
		}
	}


	//read int from file stream
	bool FileIO::readBool (std::ifstream* inInputFile) {
		char boolValue;
		inInputFile->read((byte*)&boolValue, sizeof(boolValue));
		if (boolValue == 'T') {
			return true;
		}
		return false;
	}

	//booleans are written to file as a char/single byte
	//Just using T and F for the hell of it is. Could be any char as long
	//as it matches in the readBool method.
	void FileIO::writeBool (std::ofstream* inOutFile, bool inBool) {
		char boolValue;
		if (inBool) {
			boolValue = 'T';
		} else {
			boolValue = 'F';
		}
		inOutFile->write((const char*)&boolValue, sizeof(boolValue));
	}



	//read int from file stream
	int FileIO::readInt (std::ifstream* inInputFile) {
		int value;
		inInputFile->read((byte*)&value, sizeof(int));
		return value;
	} 

	//write int to file stream
	void FileIO::writeInt (std::ofstream* inOutFile, int inInt) {
		inOutFile->write((const char*)&inInt, sizeof(inInt));
	}

	//read int from file stream
	//this is mainly here to read/write the save time
	long int FileIO::readLongInt (std::ifstream* inInputFile) {
		long int value;
		inInputFile->read((byte*)&value, sizeof(long int));
		Debug::info("readLingInt value: ");
		//std::cout << value << "\n";
		return value;
	}

	//write int to file stream
	//write save time as long int
	void FileIO::writeLongInt (std::ofstream* inOutFile, long int inInt) {
		inOutFile->write((const char*)&inInt, sizeof(inInt));
	}


	//read a string from file stream
	std::string FileIO::readString (std::ifstream* inInputFile, int inLen) {
		char buffer[inLen + 1];
		inInputFile->read((char*)&buffer, inLen);
		buffer[inLen] = '\0';
		std::string readStr(buffer);
		return readStr;
	}

	//writes a string to the file stream
	void FileIO::writeString (std::ofstream* inOutputFile, std::string* inString) {
		const char* c_str = inString->c_str();
		inOutputFile->write(c_str, inString->length());
	}

	//read a Player details from the file stream
	//passes off to read int/string methods
	std::unique_ptr<Player> FileIO::readPlayer (std::ifstream* inInputFile) {
		Debug::info("FileIO::readPlayer() called..\n");
		int playerNameLen = readInt(inInputFile);
		Debug::info("FileIO::readPlayer(): Player name len: ");
//		std::cout << playerNameLen << "\n";		

		if (playerNameLen > 30) {
			Debug::warning("Player name too long, skipping player ");
//			std::cout << "(length is " << playerNameLen << ")\n";
			return nullptr;
		}


		std::string playerName = readString(inInputFile, playerNameLen);
		Debug::info("FileIO::readPlayer(): Player name: ");
//		std::cout << playerName << "\n";		

		int playerId = readInt(inInputFile);
		Debug::info("FileIO::readPlayer(): Player id: ");
//		std::cout << playerId << "\n";		

		bool isCompPlayer = readBool(inInputFile);
		Debug::info("FileIO::readPlayer(): Player isComp: ");
//		std::cout << isCompPlayer << "\n";

		std::unique_ptr<Player> player = std::make_unique<Player>();
		player->setName(playerName);
		player->setId(playerId);
		player->setCompPlayer(isCompPlayer);
		return std::move(player);
	}

	//write a Player details from the file stream
	//passes off to write int/string methods
	void FileIO::writePlayer (std::ofstream* inOutputFile, Player* inPlayer) {
		Debug::info("FileIO::writePlayer() called..\n");
		//write player name length
		writeInt(inOutputFile, inPlayer->getName()->length());
		//write player name
		writeString(inOutputFile, inPlayer->getName());
		writeInt(inOutputFile, inPlayer->getId());
		writeBool(inOutputFile, inPlayer->isCompPlayer());
	}
	
	//read a Game details from the file stream
	//passes off to read int/string methods
	std::unique_ptr<Game> FileIO::readGame (std::ifstream* inInputFile) {
		Debug::info("FileIO::readGame() called..\n");
		Debug::todo("Read games from save file..\n");

		//create game to save to
		std::unique_ptr<Game> game = std::make_unique<Game>();

		//read state (int)
		GameState state = static_cast<GameState>(readInt(inInputFile));
		game->setGameState(state);

		//read red player id (int)
		game->setRedPlayerId(readInt(inInputFile));

		//read white player id (int)
		game->setWhitePlayerId(readInt(inInputFile));

		//read red player score (int)
		game->setRedPlayerScore(readInt(inInputFile));

		//read white player score (int)
		game->setWhitePlayerScore(readInt(inInputFile));

		//read gameboard state
		game->setGameBoard(std::move(readGameBoard(inInputFile)));

		//read save time/date (long?)
		time_t saveTime = static_cast<time_t>(readLongInt(inInputFile));
		game->setSaveTime(saveTime);

		//move game data back to calling method
		return std::move(game);
	}
	
	//write a Game details from the file stream
	//passes off to write int/string methods
	void FileIO::writeGame (std::ofstream* inOutputFile, Game* inGame) {
		Debug::info("FileIO::writeGame() called..\n");

		//write state (int)
		writeInt(inOutputFile, inGame->getGameState());

		//write red player id (int)
		writeInt(inOutputFile, inGame->getRedPlayerId());

		//write white player id (int)
		writeInt(inOutputFile, inGame->getWhitePlayerId());

		//write red player score (int)
		writeInt(inOutputFile, inGame->getRedPlayerScore());

		//write white player score (int)
		writeInt(inOutputFile, inGame->getWhitePlayerScore());

		//write gameboard state
		writeGameBoard(inOutputFile, inGame->getGameBoard());

		//write save time/date
		time_t prevSaveTime = inGame->getSaveTime();
		long int saveTime; 
		if (prevSaveTime == 0) {
			saveTime = static_cast<long int> (time(NULL));
		} else {	
			saveTime = static_cast<long int> (prevSaveTime);
		}
		writeLongInt(inOutputFile, saveTime);

	}


	//first step of reading the saved data
	//read how many players are stored
	//loop that many times to load players
	//return vector of player data
	std::unique_ptr<std::vector<std::unique_ptr<Player>>> FileIO::readPlayers (std::ifstream* inInputFile) {
		


		//vector to store players read and pass to AppData obj after
		std::unique_ptr<std::vector<std::unique_ptr<Player>>> players;
		players = std::make_unique<std::vector<std::unique_ptr<Player>>>();

		//read num of plays stored
		int numPlayerProfiles = readInt(inInputFile);
//		Debug::info("FileIO::readPlayers(): Num profiles found: ");
		//std::cout << numPlayerProfiles << "\n";	
	
		if (numPlayerProfiles > 100) {
			IO::print("WARNING: ", YELLOW);
			IO::print("There are more than 100 profiles found. File is likely corrupt.\n");
			return players;
		}

		//loop numPlayerProfiles times to retrieve player data
		//push onto vector
		for (int i = 0; i < numPlayerProfiles; i++) {
			players->push_back(readPlayer(inInputFile));
		}

		//will return empty vector if no players would stored
		return players;
	}

	void FileIO::writePlayers (std::ofstream* inOutputFile, std::vector<std::unique_ptr<Player>>* inPlayers) {
		Debug::info("FileIO::writePlayers() called..\n");	
//		write num players first..
		int numPlayers = inPlayers->size();
		writeInt(inOutputFile, numPlayers);
		for (auto iter = inPlayers->begin(); iter < inPlayers->end(); iter++) {
			writePlayer(inOutputFile, iter->get());	
		}
		
	}

	//as above, reads the games from the saved data in AppData
	//reads how many games were saved
	//the loops that many times to read each game, storing in a vector
	//returns vector of game data
	std::unique_ptr<std::vector<std::unique_ptr<Game>>> FileIO::readGames (std::ifstream* inInputFile) {
		Debug::info("FileIO::readGames() called..\n");	
	
		//read num of plays stored
		int numGamesSaved = readInt(inInputFile);
//		std::cout << "num saved games: " << numGamesSaved << "\n";
		if (numGamesSaved > MAX_NUM_SAVES) {
			IO::warning("Max number of saves has been reached.");
			numGamesSaved = MAX_NUM_SAVES;
		}
		
		//vector to store players read and pass to AppData obj after
		std::unique_ptr<std::vector<std::unique_ptr<Game>>> games;
		games = std::make_unique<std::vector<std::unique_ptr<Game>>>();

		//loop numPlayerProfiles times to retrieve player data
		//push onto vector
		for (int i = 0; i < numGamesSaved; i++) {
			try {
				std::unique_ptr<Game> game = readGame(inInputFile);
				if (game != nullptr) {
					games->push_back(std::move(game));
				} else {
					break;
				}
			} catch (std::exception& failedToLoadException) {
				IO::error("Failed to load a saved game (details unavailable)\n");
			}
		}

		//will return empty vector if no players would stored
		return games;
	}

	//write all games in the app data
	void FileIO::writeGames (std::ofstream* inOutputFile, std::vector<std::unique_ptr<Game>>* inGames) {
		Debug::info("FileIO.writeGames() called..\n");
		int numGames = inGames->size();

		//write number of games to save/load later
		writeInt(inOutputFile, numGames);

		//loop games, send to writeGame to write to file
		for (auto iter = inGames->begin(); iter < inGames->end(); iter++) {
			writeGame(inOutputFile, iter->get());
		}

	}


	//takes in a filename string
	//loads the data into an AppData obj and returns.
	std::unique_ptr<AppData> FileIO::loadAppData (std::string inFileName) {
		Debug::info("IO::loadAppData() called..\n");
		//AppData obj to store data and return
		std::unique_ptr<AppData> appData = std::make_unique<AppData>();

		//get the file stream and check loaded ok
		std::unique_ptr<std::ifstream> inInputFile = getInputStream(inFileName);
		if (!inInputFile->good()) {
			IO::print("Error: ", RED);
			IO::print("Unable to open file '" + inFileName + "'.\n");
			return nullptr;
		}
	
		//var to store loaded players
		std::unique_ptr<std::vector<std::unique_ptr<Player>>> players;
		//load the players...
		players = readPlayers(inInputFile.get());
		//store to the AppData obj
		appData->setPlayers(std::move(players));

		//make a vector and read the games into it
		std::unique_ptr<std::vector<std::unique_ptr<Game>>> games;
		//read games into the vector
		games = readGames(inInputFile.get());
		//store to the AppData obj
		appData->setGames(std::move(games));

		//close off file after use.
		inInputFile->close();

		//return populated appData
		return std::move(appData);
	}


	 /*	saving process:
	  *
	 *	- get players vector size
	 *		=> write to file (int)
	 *	- for each player
	 *		- get lenght of name
	 *			=> write to file (int)
	 *		- get name
	 *			=> write to file (char*)
	 *		- get player id
	 *			=> write to file (int)
	 *	- get saved games vector size
	 *		=> write to file (int)
	 *	- for each saved game
	 *		- get cell contents as an array of ints
	 *			=> write to file (int[])
	 *		- get red player id
	 *			=> write to file (int)
	 *		- get red player score
	 *			=> write to file (int)
	 *		- get white player id
	 *			=> write to file (int)
	 *		- get white player score
	 *			=> write to file (int)
	 */

	void FileIO::saveAppData (std::string inFileName, AppData* inAppData) {
		Debug::info("FileIO::saveAppData() called..\n");
		std::unique_ptr<std::ofstream> outputFile = getOutputStream(inFileName);
		if (outputFile == nullptr) {
			IO::print("ERROR: ", RED);
			IO::print("Could not open file '" + inFileName + "' for saving.\n");
			return;
		}
		writePlayers(outputFile.get(), inAppData->getPlayers());
		writeGames(outputFile.get(), inAppData->getGames());
		outputFile->close();
	
	}

