/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>

class AppData;

enum color {
	WHITE, BLUE, RED, YELLOW, GREEN, MAGENTA, CYAN
};

class IO {

private:
	
	static void setColor (enum color inColor);

protected:
public:


	//basic print number and string
	static void print (int inInt);
	static void print (std::string inString);

	//print number or string as coloured
	static void print (int inInt, enum color inColor);
	static void print (std::string inString, enum color inColor);

	//pre-appends the string message with coloured tag
	//DO NOT USE FOR DEBUGGING. Use cloned Debug.h for debug messages
	static void success(std::string inString);
	static void info (std::string inString);
	static void note (std::string inString);
	static void warning (std::string inString);
	static void error (std::string inString);

	//other methods for IO
	static std::string getInput ();
	static std::unique_ptr<AppData> loadAppData (std::string inFileName);
	static void saveAppData (std::string inFileName, AppData* inAppData);
	static std::unique_ptr<AppData> getEmptyAppData ();
};
