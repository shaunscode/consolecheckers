/*
*	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include <memory>

#include "./BaseModel.h"


//seperate to GameModel class so other classes can use it
enum GameState {
	EMPTY,
	WAITING,
	READY,
	RED_TURN,
	WHITE_TURN,
	OVER
};

enum GameModelState {
	STARTING,
	PLAYING,
	SAVING,
	FINISHED
};

class AppData;
class Game;

class GameModel: public BaseModel {

private:

	std::unique_ptr<Game> game;

protected:
public:

	GameModel (AppData& inAppData):
		BaseModel(inAppData),
		game(nullptr) {
		setModelState(STARTING);
	//	Debug::info("GameModel constructed.\n");
	}

	~GameModel () {
	//	Debug::info("GameModel deconstructed.\n");
	}

	//sets state, only accepts GameState
	//outside classes can see this
	//it then sets the protected BaseModel modelState value
	void setState (GameState& inState) {
		modelState = inState;
	}

	void setGame (std::unique_ptr<Game> inGame) {
		game = std::move(inGame);
	}

	Game* getGame () {
		return game.get();
	}

};
