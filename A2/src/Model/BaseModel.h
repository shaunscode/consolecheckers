/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include <memory>
#include <vector>

#include "../App/AppData.h"
#include "../Game/Player.h"

class BaseModel {
private:


protected:
	//state is an int to allow use in derived classes.
	int modelState;
	AppData& data;

public:

	BaseModel (AppData& inAppData):
		modelState(0),
		data(inAppData) {
//		Debug::info("BaseModel constructed.\n");
	}

	virtual ~BaseModel () {
//		Debug::info("BaseModel deconstructed.\n");
	}

	virtual int getModelState () {
		return modelState;
	}

	virtual void setModelState (int inState) {
		modelState = inState;
	}

	virtual AppData& getAppData () {
		return data;
	}


	std::vector<std::unique_ptr<Player>>* getPlayers () {
		return data.getPlayers();

	}

	Player* getPlayerById (int inId) {

		for (auto iter = getPlayers()->begin(); iter != getPlayers()->end(); iter++) {
			if (iter->get()->getId() == inId) {
				return iter->get();
			}
		}
		return nullptr;
	}

	Player* getPlayerByIndex (int inIndex) {
		if (inIndex > getPlayers()->size()) {
			return nullptr;
		}
		return getPlayers()->at(inIndex).get();
	}
};
