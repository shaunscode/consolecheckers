/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "BaseView.h"
#include "../Game/Game.h"

class GameView: public BaseView {

private:

void printSolidRow () {
	for (int col = 0; col < GAME_BOARD_SIZE + 1; col++) {
		std::cout << "----";
	}
	std::cout << "\n";
}

void printBoardHeader () {
	std::cout << "\n   |";
	for (int col = 0; col < GAME_BOARD_SIZE; col++) {
		std::cout << " " << (col + 1) << " |";
	}
	std::cout << "\n";
	printSolidRow();

}



void printBoardPiece (int inPiece){

	PieceType piece = static_cast<PieceType>(inPiece);
	

	switch(piece) {
		case PieceType::NONE:
		IO::print(" ");
		break;
		case PieceType::RED:
		IO::print("o", RED);
		IO::print("");	//quick way to set colour back to white
				//cos std::cout is used to print the board
		break;
		case PieceType::WHITE:	//black piece = white print out
		IO::print("o", WHITE);
		break;
		case PieceType::RED_KING: //red king
		IO::print ("K",RED);
		IO::print("");	//quick way to set colour back to white
				//cos std::cout is used to print the board
		break;
		case PieceType::WHITE_KING: //black king
		IO::print("K", WHITE);
		break;
		default:	
		IO::print("X", CYAN);	//debugging, incase an unknown 
					//piece makes it's way in?
		break;
		
	}
}

void printBoardContent (GameBoard& inBoard) {

	for (int row = 0; row < GAME_BOARD_SIZE; row++) {
		std::cout << " " << (row + 1) << " |";
		for (int col = 0; col < GAME_BOARD_SIZE; col++) {
			std::cout << " ";
			printBoardPiece(inBoard[row][col]);
			std::cout << " |";
		}
		std::cout << "\n";
		printSolidRow();
	}
}


void showBoard(Game* inGame) {

	std::cout << "\n";
	printBoardHeader();
	printBoardContent(*inGame->getGameBoard());
	std::cout << "\n";
}



void showScore(Game* inGame) {
	IO::print("*** ", CYAN);
	IO::print("Red Player: ");
	IO::print(inGame->getRedPlayerScore());
	IO::print(" - ", CYAN);
	IO::print("White Player: ");
	IO::print(inGame->getWhitePlayerScore());
	IO::print(" ***", CYAN);
	IO::print(""); //sets back to white as gameboard 
			//uses std::cout to be drawnb
}







/*
	void showBoard (Game* inGame) {
		GameBoard& board = *inGame->getGameBoard();
		for (int i = 0; i < GAME_BOARD_SIZE; i++) {
			for (int j = 0; j < GAME_BOARD_SIZE; j++) {
				int value = board[i][j];
				std::cout << value << " ";
			}
			std::cout << "\n";

		}

		Debug::todo("Show the game board!\n");
	}
*/
	void showGame (Game* inGame) {
		
		//show the state of the board asdasd
		showScore(inGame);
		showBoard(inGame);
		showGetMoveInput();
		switch(inGame->getGameState()) {

			case(RED_TURN):
				IO::print("Red player's turn, please enter a move..\n");
				break;
			case(WHITE_TURN):
				IO::print("White player's turn, please enter a move..\n");
				break;
			case(OVER):
				Debug::info("GameView::display(): state is OVER\n");
				return;
			case(READY):
			case(EMPTY):
			case(WAITING):
			default:
				Debug::info("GameView::display(): state is DEFAULT, READY, EMPTY, WAITING..\n");
				//dont bother displaying the game board for these states
				return;
		}

	}

	void showStarting () {
		IO::print("Game has started!\n");
		IO::info("Type --help instead of entering a move to display alternative options!\n");
	}

	void showGameNotReady () {
		IO::error("The game that is starting appears to be not ready!Returning to menu to try again!\n");
	}

/*
	void showBoardBasic(GameBoard& inBoard) {
		for (int i = 0; i < GAME_BOARD_SIZE; i++) {
			for (int j = 0; j < GAME_BOARD_SIZE; j++) {
				std::cout << "" << (int)inBoard[i][j];
			}
			std::cout << "\n";
		}		
	}
*/
protected:
public:
	

	void showGetMoveInput () {
		IO::print("Please enter a move of the format x1,y1 - x2,y2 where each is a digit between 1 and 8\n");
		IO::print("(type S to save this game and L to replace this game with a previously saved game)\n");
	}

	void display (BaseModel& inModel) {
	
		Debug::info("GameView::display() called..\n");
		GameModel& gameModel = dynamic_cast<GameModel&>(inModel);
		
//		showBoardBasic(gameModel.getGame()->getGameBoard());

		//Debug::info("MenuView::display() called..\n");
		int gameModelState =  gameModel.getModelState();
		GameState gameState = gameModel.getGame()->getGameState();
		if(gameState == READY) {
			gameModel.getGame()->setGameState(RED_TURN);
			showStarting();
			gameModel.setModelState(FINISHED);
		}
		
		gameState = gameModel.getGame()->getGameState();
		if (gameState == RED_TURN || gameState == WHITE_TURN) {
			showGame(gameModel.getGame());
		} else {
			showGameNotReady();
			gameModel.getGame()->setGameState(OVER);
		}
	}
}; 
