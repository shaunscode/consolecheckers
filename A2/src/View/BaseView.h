/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "../Model/BaseModel.h"
#include "../App/IO.h"
#include "../Game/Player.h"
#include "../Game/Game.h"


class BaseView {

private:
protected:
public:

	BaseView () {
//		Debug::info("BaseView constructed.\n");
	}

	virtual ~BaseView () {
//		Debug::info("BaseView deconstructed.\n");
	}

	//user can override this method to return any string 
	//however they want (eg. gui screen click->return button id)
	virtual std::string getInput () {
		return IO::getInput();
	}

	virtual void display (BaseModel& model) = 0;

};
