/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#include <memory>

#include "./MenuController.h"
#include "../Input/MenuInput.h"
#include "../Model/MenuModel.h"

#include "../Commands/AppCmds/AppQuit.h"
#include "../Commands/AppCmds/AppLoadGame.h"
#include "../Commands/AppCmds/AppSaveAndQuit.h"
#include "../Game/Game.h"
#include "./BaseController.h"

	void MenuController::updateModel () {
//		Debug::info("MenuController::updateModel() called..\n");
	}

	bool MenuController::isNameValid (std::string inName) {
		AppData& appData = model->getAppData();
		std::vector<std::unique_ptr<Player>>* players;
		players = appData.getPlayers();
		for (auto iter = players->begin(); iter != players->end(); iter++) {
			if (iter->get()->getName()->compare(inName) == 0) {
				return false;
			}	
		}
		return true;
	}

	void MenuController::showWelcome() {
		model->setModelState(WELCOME);
	}

	void MenuController::showAddPlayer () {
		model->setModelState(ADD_PLAYER);
	}


	//get a new player id, start at 1000 for the hell of it
	//go over current Id's to ensure it's not taken
	//otherwise pass it back
	int MenuController::getNewPlayerId () {
		int newId = 1000;
		bool available = true;
		
		AppData& appData = model->getAppData();
		std::vector<std::unique_ptr<Player>>* players;
		players = appData.getPlayers();
		do {
			available = true;
			newId++;
			for (auto iter = players->begin(); iter != players->end(); iter++) {
				if (iter->get()->getId() == newId) {
					available = false;
					break;
				}	
			} 
		} while (!available);
		return newId;
	}


	//attempt to add a new player
	//ensure name isn't taken, then get an new id and add to AppData players vector
	//update model state and update view to show validation or not validated
	//return to welcome screen if valid
	void MenuController::setPlayerName (std::string inName) {
//		Debug::info("MenuController::setPlayerName() called, input: " + inName);

		//is name valid
		if (isNameValid(inName)) {

			//get a new id for them
			int newId = getNewPlayerId();	
			//add them to appData

			std::unique_ptr<Player> player = std::make_unique<Player>();
			player->setName(inName);
			player->setId(newId);

			//convenience method to add anoter player
			model->getAppData().addPlayer(std::move(player));
			
			//show user name is valid and go back to welcome
			model->setModelState(NAME_VALID);
			updateView();
			//return to welcome menu after success	
			showWelcome();
			return;
		}
		
		//name was taken, show invalid and return to 'enter name' screen
		model->setModelState(NAME_INVALID);
		updateView();
		showAddPlayer();
	}

	//get game obj from the MenuModel
	//MenuModel will return a new game obj if it's yet to be created.
	Game* MenuController::getGame () {
		//get the model to get the game
		MenuModel* menuModel = dynamic_cast<MenuModel*>(model.get());
		return menuModel->getGame();
	}

	//set a player in the game that will be passed back to play
	void MenuController::setPlayer (std::string inIndex) {
		//get input index as int	
		int index = atoi(inIndex.c_str());

		//ensure index is viable
		MenuModel* menuModel = dynamic_cast<MenuModel*>(model.get());
		Player* player = menuModel->getPlayerByIndex(index);
		if (player == nullptr) {
			IO::error("Selected index was not located!\n");
			return;
		}

		//get the current game data being populated for playing.
		Game* game = getGame();

		//check by playerId if player is already in the game waiting
		if (game->getGameState() == WAITING && player->getId() == game->getRedPlayerId()) {
			IO::error("Selected index was not available!\n");
			return;
		}
		//game empty? no players, so fill red player spot and set to waiting
		if (game->getGameState() == EMPTY) {
			game->setRedPlayerId(player->getId());
			game->setGameState(WAITING);
			Debug::info("MenuController::setPlayer(): setting red player with id '" + std::to_string(player->getId()) + "'\n");
		//waiting? one player there, so populate other and load the game
		} else if (game->getGameState() == WAITING) {
			game->setWhitePlayerId(player->getId());
			game->setGameState(READY);

			Debug::info("MenuController::setPlayer(): setting white player with id '" +  std::to_string(player->getId()) + "'\n");
			//take game will take ownership of the game obj, passing it to the game
			//via an AppLoadGame cmd.
			doPlayGame(menuModel->takeGame());
		}

	}

	void MenuController::showSelectPlayer () {
		MenuModel& menuModel = dynamic_cast<MenuModel&>(*model);

		if (menuModel.getPlayers()->size() < 2) {
			IO::error("Not enough players to player a game! (Less than two!)\n");
			return;
		}
		model->setModelState(SELECT_PLAYER);
//		Debug::info("MenuController::showSelectPlayer() called..\n");
	}

	void MenuController::showLoad () {
//		Debug::info("MenuController::showLoad() called..\n");
		model->setModelState(LOAD);
	}

	void MenuController::showLoadSuccess () {
		model->setModelState(LOAD_SUCCESS);
	}

	//this is an action to take just like quitting, but save before quitting.
	void MenuController::doSaveAndExit () {
		setNextAppCmd(std::make_unique<AppSaveAndQuit>(app));
		model->setModelState(EXIT);
	}

	void MenuController::doExit () {
//		Debug::warning("MenuController::dodoExit() being called..\n");
		model->setModelState(EXIT);
		setNextAppCmd(std::make_unique<AppQuit>(app));
	}

	std::unique_ptr<Game> MenuController::getCopyOfGame (Game* inGame) {
		return std::make_unique<Game>(inGame);
	}

	void MenuController::doLoadGame (int inGameIndex) {
		//get the game from the saves by index
		auto games = model->getAppData().getGames();
		Game* game = games->at(inGameIndex).get();

		//because the AppData owns the saved games, we need to make a copy
		//of it in order to pass to the Game MVC set to play with.
		//a ref could possibly change the actual saved game content within the AppData
		//a copy is made so further saving can be done without affecting the original
		//saved game.
		doPlayGame(std::make_unique<Game>(game));


	}

	//takes the GameData passed in and passes it onto the App controller to
	//load the Game MVC set with the game data.
	//Game MVC set then owns this game data.
	//This is so games passed in from the menu aren't left owned by the menu,
	//and games loaded from the AppData are seperated from the AppData and
	//become a new game in themselves.
	void MenuController::doPlayGame (std::unique_ptr<Game> inGame) {
		Debug::info("MenuController::playGame() called..\n");

		//make an app cmd to load the game
		std::unique_ptr<AppLoadGame> loadGame = std::make_unique<AppLoadGame>(app);

		//get model as menu mode and retrieve the game, then put into command
		//MenuModel* menuModel = dynamic_cast<MenuModel*>(model.get());

		loadGame->setGame(std::move(inGame));

		//set the app next command for next loop to load the game.
		setNextAppCmd(std::move(loadGame));

		//put menu model back to show welcome before we go
		showWelcome();

	}

	void MenuController::buildInputMaps () {
		if (input == nullptr) {
			Debug::error("MenuController::buildInputMaps(): there is no input to build for\n");
			return;
		}
		MenuInput* menuInput = dynamic_cast<MenuInput*>(input.get());
		menuInput->buildInputMaps(*this);
	}
