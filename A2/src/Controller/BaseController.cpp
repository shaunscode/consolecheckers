/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#include "./BaseController.h"

	void BaseController::updateView () {
		view->display(*model.get());
	}

		

	std::unique_ptr<BaseAppCmd> BaseController::getNextAppCmd () {
//		 Debug::info("MainController::getNextAppCmd() called..\n");
		 return std::move(nextCmd);
	}

	void BaseController::setNextAppCmd (std::unique_ptr<BaseAppCmd> inNextCmd) {
//		 Debug::info("MainController::setNextAppCmd() called..\n");
		 nextCmd = std::move(inNextCmd);
	}

	//just executes the command returned from, handling the input
	void BaseController::handleCmd(BaseCmd* inCmd) {
		if (inCmd != nullptr) {
			inCmd->execute();
		}
	}

	//by default BaseController::update() calls this method for input
	//this method calls the view's getInput() method
	//Derived controllers can override this virtual method for the App
	//to get input from wherever it wants.
	//eg. GameController will override and implement alternating between
	//Player::getInput() so that computer oppentents are handled just the same
	//as human players.
	std::string BaseController::getInput () {
		return view->getInput();
	}

	//this virutal method is for dervied controllers to override and perform
	//anything controller specific that is necessary
	//typically include an updateView() call in this within the derived class
	void BaseController::updateController () {
		//method stub for overriding.	
	}

	void BaseController::updateModel () {
		//method stub for overriding.
	}
	//renders screen according to the model
	std::unique_ptr<BaseAppCmd> BaseController::update () {

		//update view with current model data
		updateView();

		//get input, wherever from we're using strings as the key
		//between user input and the controller handing it
		std::string userInput = getInput();

		//Input::handleInput takes in the model state and the user input string
		//retrieved above
		//it then returns a BaseCmd from the Input class as a result
		BaseCmd* cmd = input->handleInput(model->getModelState(), userInput);

		//Basically handleCmd will just run the .execute() method of the command
		//which encapsulates everything it needs
		handleCmd(cmd);

		//not sure if this is necessary? Controller kinda updates model directly..
		updateModel();

		//a change for the controller to handle current state and make changes
		//prior to the next updateView and next getInput calls.
		//unless a derrived controller has overriden this method
		//nothing will happen in the call here to BaseController::updateController()
		updateController();

		//Controllers have access to the App obj and it's setNextAppCmd() method
		//in order to tell the App to load a diff MVC set.
		//really it could use the command patter to get it to do whatever here.
		return getNextAppCmd();
	}


	void BaseController::setModel (std::unique_ptr<BaseModel> inModel) {
//		Debug::info("BaseControler::setModel() called..\n");
		model = std::move(inModel);
	}


	void BaseController::setView (std::unique_ptr<BaseView> inView) {
//		Debug::info("BaseControler::setView() called..\n");
		view = std::move(inView);
	}

	void BaseController::setInput (std::unique_ptr<BaseInput> inInput) {
//		Debug::info("BaseControler::setInput() called..\n");
		input = std::move(inInput);
	}
