/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *
 */

#pragma once

//String stream for converting the input to a string
#include <sstream>
#include <memory>
#include <map>

#include "./BaseInput.h"
#include "../Commands/MenuCmds/BaseMenuCmd.h"
#include "../Model/MenuModel.h"


class BaseCmd;
class MenuController;



typedef std::map<int, std::unique_ptr<BaseMenuCmd>> MenuCmdMap;


class MenuInput: public BaseInput {

private:

	std::map<MenuState, std::unique_ptr<MenuCmdMap>> cmdsMap;

protected:
public:

	MenuInput () {
//		Debug::info("MenuInput constructed\n");
	}
	
	~MenuInput () {
//		Debug::info("BaseInput deconstructed.\n");
	}
	
	BaseCmd* handleInput (int inState, std::string inInput);

	//returns unique ptr to a map,
	//of int mapped to unique ptr's of MenuCmd
	std::unique_ptr<MenuCmdMap> getWelcomeCmdMap (MenuController& menu);
	std::unique_ptr<MenuCmdMap> getAddPlayerCmdMap (MenuController& menu);
	std::unique_ptr<MenuCmdMap> getSelectPlayerCmdMap (MenuController& menu);
	std::unique_ptr<MenuCmdMap> getEnterNameCmdMap (MenuController& menu);
	std::unique_ptr<MenuCmdMap> getLoadCmdMap (MenuController& menu);
	void buildInputMaps (MenuController& menu);
};
