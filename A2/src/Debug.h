/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include <iostream>
#include <string>

//#include "./App/IO.h"

enum class DebugColor {
	WHITE, BLUE, RED, YELLOW, GREEN, MAGENTA, CYAN
};


class Debug {

public:
	static void setColor (enum DebugColor inColor) {
		switch (inColor) {
			case (DebugColor::CYAN):
//				std::cout << "\033[36m";
				break;
			case (DebugColor::MAGENTA):
//				std::cout << "\033[35m";
				break;
			case (DebugColor::RED):
///				std::cout << "\033[31m";
				break;
			case (DebugColor::GREEN):
///				std::cout << "\033[32m";
				break;
			case (DebugColor::BLUE):
///				std::cout << "\033[34m";
				break;
			case (DebugColor::YELLOW):
///				std::cout << "\033[33m";
				break;
			default:	//WHITE
				std::cout << "\033[37m";
		}
	}

	static void print (std::string inString) {
		setColor(DebugColor::WHITE);
//		std::cout << inString;
	}

	static void print (std::string inString, enum DebugColor inColor) {
		setColor(inColor);
//		std::cout << inString;
	}


	static void success(std::string inString) {
		print("SUCCESS: ", DebugColor::GREEN);
			print(inString, DebugColor::WHITE);

		}

	static void todo(std::string inString) {
			print("\tTODO: ", DebugColor::MAGENTA);
		print(inString, DebugColor::WHITE);

		}

	static void info (std::string inString) {
	print("INFO: ", DebugColor::BLUE);
	print(inString, DebugColor::WHITE);
	}

	static void warning (std::string inString) {
		print("** WARNING: ", DebugColor::YELLOW);
	print(inString, DebugColor::WHITE);
	}

	static void error (std::string inString) {
		print("### ERROR: ", DebugColor::RED);
		print(inString, DebugColor::WHITE);
	}
};
