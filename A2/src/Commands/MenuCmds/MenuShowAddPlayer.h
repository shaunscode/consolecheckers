/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseMenuCmd.h"

class MenuShowAddPlayer: public BaseMenuCmd {
private:
protected:
public:

	MenuShowAddPlayer (MenuController& inController):
		BaseMenuCmd(inController) {
	}

	void execute () {
		Debug::success("MenuShowAddPlayer::execute() is being called!");
		menu.showAddPlayer();
	}



};
