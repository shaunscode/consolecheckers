/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseAppCmd.h"

class AppQuit: public BaseAppCmd {
private:
protected:
public:

	AppQuit(App& inApp):
		BaseAppCmd(inApp) {
	}

	void execute () {
		app.quit();
	}

};
