/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "../BaseCmd.h"

//forward declaration
class App;

class BaseAppCmd: public BaseCmd {
private:
protected:
	App& app;
public:

	BaseAppCmd (App& inApp):
		app(inApp) {
	}
	
	virtual ~BaseAppCmd () {};

};
