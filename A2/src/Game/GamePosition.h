/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *
 */

#pragma once
#include <iostream>
#include "../Debug.h"


class GamePosition {

private:
	int col;
	int row;
protected:
public:

	GamePosition ():
		col(-1),
		row(-1) {
	}
/*
	GamePosition (GamePosition& inPos):
		x(inPos.getX(),
		y(inPos.getY() {
	}
*/
	GamePosition (int inRow, int inCol):
		col(inCol),
		row(inRow) {
//		Debug::info("GamePosition created, ");
//		std::cout << "row/col: " << row << "/" << col << "\n";
	}
	
	bool operator== (GamePosition& inPos) {
		if (inPos.getRow() == row &&
			inPos.getCol() == col) {
			return true;
		}
		return false;
	}


	GamePosition operator- (GamePosition& inPos) {
		GamePosition retPos;
		retPos.setRowCol(row - inPos.getRow(), col - inPos.getCol());
		return retPos;
	}

	GamePosition operator+ (GamePosition& inPos) {
		GamePosition retPos;
		retPos.setRowCol(row + inPos.getRow(), col + inPos.getCol());
		return retPos;
	}



	~GamePosition () { }

	void setRowCol (int inRow, int inCol) {
		row = inRow;
		col = inCol;
	}
	
	int getCol () {
		return col;
	}

	int getRow () {
		return row;
	}


};
