/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */


#include "./Game.h"
#include "../Debug.h"
#include "../Model/GameModel.h"
#include "./GamePosition.h"

Game::Game ():
	state(EMPTY),
	redPlayerId(-1),
	whitePlayerId(-1),
	redPlayerScore(0),
	whitePlayerScore(0),
	saveTime(0),
	board(std::make_unique<GameBoard>()) {
	Debug::info("Game constructed.\n");
	initGamePieces();

}

//copy consturctor
Game::Game(Game* inGame):
	state(inGame->getGameState()),
	redPlayerId(inGame->getRedPlayerId()),
	whitePlayerId(inGame->getWhitePlayerId()),
	redPlayerScore(inGame->getRedPlayerScore()),
	whitePlayerScore(inGame->getWhitePlayerScore()),
	saveTime(inGame->getSaveTime()),
	board(std::make_unique<GameBoard>()) {
	Debug::info("Game constructed, as a copy.\n");
	copyGameBoard(inGame->getGameBoard());
}

Game::~Game () {
	Debug::info("Game deconstructed.\n");

}

void Game::setRedPlayerId (int inPlayer) {
	redPlayerId = inPlayer;
}

int Game::getRedPlayerId () {
	return redPlayerId;
}

void Game::setWhitePlayerId (int inPlayer) {
	whitePlayerId = inPlayer;
}

int Game::getWhitePlayerId () {
	return whitePlayerId;
}

void Game::incRedPlayerScore () {
	redPlayerScore++;
}

void Game::setRedPlayerScore (int inScore) {
	redPlayerScore = inScore;
}

int Game::getRedPlayerScore () {
	return redPlayerScore;
}

void Game::incWhitePlayerScore () {
	whitePlayerScore++;
}

void Game::setWhitePlayerScore (int inScore) {
	whitePlayerScore = inScore;
}

int Game::getWhitePlayerScore () {
	return whitePlayerScore;
}

void Game::setGameState (GameState inGameState) {
	state = inGameState;
}

GameState Game::getGameState () {
	return state;
}

void Game::setSaveTime (time_t inTime) {
	saveTime = inTime;
}

time_t Game::getSaveTime () {
	return saveTime;
}

GameBoard* Game::getGameBoard () {
	return board.get();
}

void Game::setGameBoard (std::unique_ptr<GameBoard> inBoard) {
	board = std::move(inBoard);
}

void Game::copyGameBoard(GameBoard* inBoard) {
	Debug::info("Game::copyGameBoard() called..\n");

	for (int row = 0; row < GAME_BOARD_SIZE; row++) {
		for (int col = 0; col < GAME_BOARD_SIZE; col++) {
			(*board)[row][col] = (*inBoard)[row][col];
		}
	}

}

std::unique_ptr<std::vector<GamePosition>> Game::getRedPieces () {
	std::unique_ptr<std::vector<GamePosition>> pieces;
	pieces = std::make_unique<std::vector<GamePosition>>();
	for (int col = 0; col < GAME_BOARD_SIZE; col ++) {
		for (int row = 0; row < GAME_BOARD_SIZE; row++) {
			if ((*board)[row][col] == (int)PieceType::RED || (*board)[row][col] == (int)PieceType::RED_KING) {
				pieces.get()->emplace_back(GamePosition(row, col));
			} 
		}
	}
	return std::move(pieces);
}

std::unique_ptr<std::vector<GamePosition>> Game::getWhitePieces () {
	std::unique_ptr<std::vector<GamePosition>> pieces;
	pieces = std::make_unique<std::vector<GamePosition>>();
	for (int col = 0; col < GAME_BOARD_SIZE; col ++) {
		for (int row = 0; row < GAME_BOARD_SIZE; row++) {
			if ((*board)[row][col] == (int)PieceType::WHITE || (*board)[row][col] == (int)PieceType::WHITE_KING) {
				pieces.get()->emplace_back(GamePosition(row, col));
			} 
		}
	}
	return std::move(pieces);
}


void Game::initGamePieces () {

	//go over all pieces
	int piece = (int)PieceType::RED;

	//go over all board positions
	for (int col = 0; col < GAME_BOARD_SIZE; col++) {
		for (int row = 0; row < GAME_BOARD_SIZE; row++) {
			//piece already read, but if row is at the top
			//(rows 1 -> 3) than piece would be RED
			if (row < 3) {
				piece = (int)PieceType::RED;
			} else if (row > 2 && row < 5) {
				//middle row pieces are none
				piece = (int)PieceType::NONE;	
			} else if (row > 4) {
				//low rows (6 -> 8) would be black
				piece = (int)PieceType::WHITE;
			}
			
			//in iterating over the top or bottow rows
			//where pieces are starting off
			if (row < 3 || row > 4) {
				//and the row + col is an odd number
				//we can place a piece there, using the 
				//piece int as white type it would be.
				if ((row + col) % 2 != 0) {
					(*board)[row][col] = piece;
				}
			}
		}
	}
}

