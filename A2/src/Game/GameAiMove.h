/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *
 */

#pragma once
#include <sstream>i
#include "./GameMove.h"
#include "./GamePosition.h"

class GameAiMove: public GameMove {

private:
	GamePosition 
protected:
public:

	GameAiMove (GamePosition inFrom, GamePosition inTo):
		from(inFrom),
		to(inTo) {
	}

	GameAiMove (GameAiMove& inMove):
		from(inMove.getFrom()),
		to(inMove.getTo()) {
	}

	~GameAiMove () {

	};

	GamePosition& getTo () {
		return to;
	}

	GamePosition& getFrom () {
		return from;
	}

	std::string toString () {
		std::stringstream ss;
		ss << from.getCol() << "," << from.getRow() << "-" << to.getCol() << "," << to.getRow();
		return ss.str();
	}
};
