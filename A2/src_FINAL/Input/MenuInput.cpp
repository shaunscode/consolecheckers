/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *
 */

#include "./MenuInput.h"

#include "../Commands/MenuCmds/MenuDoExit.h"
#include "../Commands/MenuCmds/MenuDoSaveAndExit.h"
#include "../Commands/MenuCmds/MenuShowAddPlayer.h"
#include "../Commands/MenuCmds/MenuShowLoad.h"
#include "../Commands/MenuCmds/MenuShowSelectPlayer.h"
#include "../Commands/MenuCmds/MenuShowWelcome.h"
#include "../Commands/MenuCmds/MenuSetPlayerName.h"
#include "../Commands/MenuCmds/MenuSetPlayer.h"
#include "../Commands/MenuCmds/MenuLoadGame.h"


#include "../Model/MenuModel.h"

BaseCmd* MenuInput::handleInput (int inState, std::string inInput) {
	//WARNING: static casting this enum has potential to be unsafe.
	//Assumption is made MenuState is never set using an int and there
	//must ALWAYS be valid.
	MenuState state = static_cast<MenuState>(inState);

	//get the cmds map for the current state and
	//ensure we found one
	auto menuCmdPair = cmdsMap.find(state);
	if (menuCmdPair == cmdsMap.end()) {
		Debug::error("MenuInput::handleInput() cannot find a state in the state/cmd maps\n");
		IO::warning("Could not find the correct command map.\n");
		return nullptr;
	}

	//convert the input to an int from menu selection to determine
	//which index to retrieve from cmd map
	int inputInt = 0;
	std::stringstream ss(inInput);
	ss >> inputInt;
	
	//Handling for ADD_PLAYER
	//ADD_PLAYER cmd map has retururning to welcome at zero
	//anything other than that, expect input string to be name
	//Set inputInt to 1, which is SetPlayer call in the 
	//ADD_PLAYER cmd map.
	if (state == ADD_PLAYER && inInput.compare("0") != 0) {
		inputInt = 1;		
	}

	//Handle SELECT_PLAYER state
	//all input (numbers) call the single cmd or adding a player to the game,
	//using the input number to pull the correct player from the players
	if (state == SELECT_PLAYER || state == LOAD) {
		inputInt = 1;
	}


	MenuCmdMap* cmdMapUniquePtr = menuCmdPair->second.get();		//second is the unique ptr to states cmd map
	BaseMenuCmd* cmdPointer = cmdMapUniquePtr->find(inputInt)->second.get();	//second is the unique ptr to the command
	cmdPointer->setData(inInput);//set the input data for controller to process (eg. player name)

	return cmdPointer;

}

//When on the WELCOME screen, these are the corrosponding cmds to retrieve
//base on option selected -> map index
std::unique_ptr<MenuCmdMap> MenuInput::getWelcomeCmdMap (MenuController& menu) {

	//create a map to store cmds in
	std::unique_ptr<std::map<int, std::unique_ptr<BaseMenuCmd>>> welcomeMap;
	welcomeMap = std::make_unique<std::map<int, std::unique_ptr<BaseMenuCmd>>>();

	//add input value->cmd to retrieve
	welcomeMap->emplace(1, std::make_unique<MenuShowAddPlayer>(menu));
	welcomeMap->emplace(2, std::make_unique<MenuShowSelectPlayer>(menu));
	welcomeMap->emplace(3, std::make_unique<MenuShowLoad>(menu));
	welcomeMap->emplace(4, std::make_unique<MenuDoSaveAndExit>(menu));
	welcomeMap->emplace(5, std::make_unique<MenuDoExit>(menu));

	//return it to store
	return std::move(welcomeMap);
}


//when screen is on ADD PLAYER, these are the cmds that can be issues
//because this screen takes in a string as a name, there is special handling in
// the handleInput() method above, which retrieves MenuSetPlayerName cmd
std::unique_ptr<MenuCmdMap> MenuInput::getAddPlayerCmdMap (MenuController& menu) {
	//create a map to store cmds in
	std::unique_ptr<std::map<int, std::unique_ptr<BaseMenuCmd>>> addMap;
	addMap = std::make_unique<std::map<int, std::unique_ptr<BaseMenuCmd>>>();

	//add input value->cmd to retrieve
	addMap->emplace(0, std::make_unique<MenuShowWelcome>(menu));
	addMap->emplace(1, std::make_unique<MenuSetPlayerName>(menu));

	//return it to store
	return std::move(addMap);
}


std::unique_ptr<MenuCmdMap> MenuInput::getSelectPlayerCmdMap (MenuController& menu) {
	//create a map to store cmds in
	std::unique_ptr<std::map<int, std::unique_ptr<BaseMenuCmd>>> playersMap;
	playersMap = std::make_unique<std::map<int, std::unique_ptr<BaseMenuCmd>>>();

	//add input value->cmd to retrieve
	playersMap->emplace(0, std::make_unique<MenuShowWelcome>(menu));
	playersMap->emplace(1, std::make_unique<MenuSetPlayer>(menu));

	//return it to store
	return std::move(playersMap);
}

std::unique_ptr<MenuCmdMap> MenuInput::getLoadCmdMap (MenuController& menu) {
	//create a map to store cmds in
	std::unique_ptr<std::map<int, std::unique_ptr<BaseMenuCmd>>> saveGamesMap;
	saveGamesMap = std::make_unique<std::map<int, std::unique_ptr<BaseMenuCmd>>>();

	//add input value->cmd to retrieve
	saveGamesMap->emplace(0, std::make_unique<MenuShowWelcome>(menu));
	saveGamesMap->emplace(1, std::make_unique<MenuLoadGame>(menu));

	//return it to store
	return std::move(saveGamesMap);
}


void MenuInput::buildInputMaps (MenuController& menu) {

	cmdsMap.emplace(WELCOME, getWelcomeCmdMap(menu));
	cmdsMap.emplace(ADD_PLAYER, getAddPlayerCmdMap(menu));
	cmdsMap.emplace(SELECT_PLAYER, getSelectPlayerCmdMap(menu));
	cmdsMap.emplace(LOAD, getLoadCmdMap(menu));
}

