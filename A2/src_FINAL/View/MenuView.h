/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include <ctime>
#include <iomanip>
#include <sstream>

#include "BaseView.h"
#include "../Model/MenuModel.h"
#include "../Model/BaseModel.h"
#include "../App/AppData.h"

class MenuView: public BaseView {

private:
protected:
public:

	MenuView () {
//		Debug::info("MenuView constructed.\n");
	}

	~MenuView () {
//		Debug::info("MenuView deconstructed.\n");
	}

	void showWelcome () {
		IO::print("\n\nWelcolme to English Draughts\n");
		IO::print("-------------------------------------\n", BLUE);
		IO::print("1");
		IO::print(") ", GREEN);
		IO::print("Add Player to the system\n");
		IO::print("2");
		IO::print(") ", GREEN);
		IO::print("Play Game\n");
		IO::print("3");
		IO::print(") ", GREEN);
		IO::print("Load Game\n");
		IO::print("4");
		IO::print(") ", GREEN);
		IO::print("Save Data and Exit\n");
		IO::print("5");
		IO::print(") ", GREEN);
		IO::print("Exit Without Saving Data\n");
	}

	void showAddPlayer () {
		IO::print("\n\nAdd New Player\n");
		IO::print("-------------------------------------\n", BLUE);
		IO::print("Please enter your name..\n");
	}

	void showNameValid (std::string &inName) {
		IO::print("User \"" + inName + "\" as been successfully added to the system.\n");
	}

	void showNameInvalid (std::string &inName) {
		IO::print("Error: ", RED);
		IO::print("\"" + inName + "\" already exists in the system. \nPlease try again with another name.\n");
	}

	void showSelectPlayer (MenuModel &model) {
		IO::print("\n\nSelect Players\n");
		IO::print("-------------------------------------\n", BLUE);
		IO::print("Please select a player to add to the game\n");

		//pull players from the model
		std::vector<std::unique_ptr<Player>>* players = model.getAppData().getPlayers();

		//see if there's a player already selected from the current game in the model
		//if there is, don't display them
		Game* game = model.getGame();
		int waitingPlayerId = -1;
		if (game->getGameState() == WAITING) {
			waitingPlayerId = game->getRedPlayerId();
		}

		int index = 0;
		for (auto iter = players->begin(); iter != players->end(); iter++) {
			if (waitingPlayerId == iter->get()->getId()) {
				//because we're selecting players via the input 0->1->n index and
				//not their ID, we still need to increment the index so they line up
				//with their index in the vector that holds the players.
				//Not the most ideal solution.
				index++;
				continue;
			}
			IO::print(index++);	//print the index
			IO::print(") ", GREEN);	//green brack
			IO::print(*iter->get()->getName());	//print the name
			IO::print("\n");
		}
	}

/*
	void showSave () {
		IO::print("Save Game\n");
		IO::print("-------------------------------------\n", BLUE);
		IO::print("Here is a list of previously saved games:\n");
		Debug::todo("Loop over list of saved games\n");
		0) Fred vs Barney saved 10 Apr 2018 at 3:59 pm
		1) Paul vs Computer saved 13 Apr 2018 at 1:22 pm
		2) Paul vs Computer saved 13 Apr 2018 at 11:22 am
		N)ew saved game slot.
		IO::print("Please enter your response\n");
		Debug::todo("Get user response\n");

	}
	void showOverrideSave () {
		IO::print("Replacing <Fred vs Computer saved 12 Apr 2018 with Paul vs Computer>.\n");
		IO::print("Are you sure? (y/n)");
		IO::print("\nTODO: Get user reponse\n", GREEN);
	}

	void showSaveSuccess () {
		IO::print("game saved.\n");
	}
*/

	std::string getPlayerNameById (MenuModel& menuModel, int inPlayerId) {
		Player* player = menuModel.getPlayerById(inPlayerId);
		if (player == nullptr) {
			return "UNKONWN";
		}
		return *player->getName();

	}

	//returns a string in the format showns in the assignment specs
	//Y = year
	//b = month
	//d = date of month
	//R = house and minute
	//p = am or pm
	std::string getSaveDetailsFromTime (time_t inTime) {
	    std::tm tm = *std::localtime(&inTime);
	    std::stringstream ss;
	    ss << std::put_time(&tm, "%d %b %Y at %R %p");
	    return ss.str();
	}

	//gets a string of the game details
	//gets the player names and save time from the model
	//strings them together and returns a std::string
	std::string getGameDetails (MenuModel& menuModel, Game* game) {
		//get the names and time details
		std::string redPlayerName = *menuModel.getPlayerById(game->getRedPlayerId())->getName();
		std::string whitePlayerName = *menuModel.getPlayerById(game->getWhitePlayerId())->getName();
		std::string saveDetails = getSaveDetailsFromTime(game->getSaveTime());

		//make a str stream to combine all details it the format we want
		std::stringstream ss;
		ss << redPlayerName << " vs " << whitePlayerName << ", saved " << saveDetails;
		return ss.str();
	}

	void showLoad (MenuModel &model) {
		IO::print("\n\nLoad Previously Saved Game\n");
		IO::print("-------------------------------------\n", BLUE);
		IO::print("Here is a list of previously saved games:\n");

		//pull games from the model
		std::vector<std::unique_ptr<Game>>* games = model.getAppData().getGames();

		//go over all the games and print the details
		int index = 0;
		for (auto iter = games->begin(); iter != games->end(); iter++) {
			IO::print(index++);	//print the index
			IO::print(") ", GREEN);	//green brack
			IO::print(getGameDetails(model, iter->get()));
			IO::print("\n");
		}
		IO::print("Please select a game to load\n");		
	}

	void showLoadSuccess () {
		IO::print("Loading <Paul vs Computer saved 13 Apr 2018 at 11:22 am>\n");
	}

	void showExit () {
			IO::print("Exiting game without saving.\n");
		}

	void showSaveAndExit () {
			IO::print("Saving data and exiting game.\n");
		}

	void display (BaseModel& inModel) {
		//Debug::info("MenuView::display() called..\n");
		MenuModel& model = dynamic_cast<MenuModel&>(inModel);
		int state =  model.getModelState();
		std::string str = "valid name here";

		switch(state) {
		case(WELCOME):
			showWelcome();
			break;
		case(ADD_PLAYER):
			showAddPlayer();
			break;
		case(NAME_VALID):
			Debug::todo("get players valid name from somewhere!\n");
			showNameValid(str);
			break;
		case(NAME_INVALID):
			Debug::todo("get players valid name from somewhere!\n");
			showNameInvalid(str);
			break;
		case(SELECT_PLAYER):
			showSelectPlayer(model);
			break;
		case(LOAD):
			showLoad(model);
			break;
		case(LOAD_SUCCESS):
			showLoadSuccess();
			break;
		case(SAVE_AND_EXIT):
			showSaveAndExit();
			break;
		case(EXIT):
			showExit();
			break;
		default:
			showWelcome();
		}




	}	



};
