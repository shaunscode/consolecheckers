/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */


#include <stdio.h>
#include <vector>
#include <iostream>
#include <algorithm>

class BinaryHeap {
  private:
  std::vector<int> arr;

  protected:
  public:

  void swapValues (int val1, int val2) {
     // std::cout << "\t\tswapValues:: " << (*(arr.begin() + val1)) << "/" << (*(arr.begin() + val2));
      iter_swap(arr.begin() + val1, arr.begin() + val2);
      //std::cout << " -> " << (*(arr.begin() + val1)) << "/" << (*(arr.begin() + val2)) << "\n";
  }

   void siftUp (int index) {

    //std::cout << "siftUp(): index: " << index << "\n";
    int parentIndex = (index / 2);
    //std::cout << "siftUp(): Parent index: " << parentIndex << "\n";
    if (arr.at(index) > arr.at(parentIndex)) {
        swapValues(index, parentIndex);
        siftUp(parentIndex);
	}
  }

  void siftDown (int index) {
    //print();
    int leftIndex = (index * 2) + 1;
    int rightIndex = leftIndex + 1;
    int indexValue = arr.at(index);
    int leftValue = -1;
    int rightValue = -1;

     if (!(leftIndex > arr.size() - 1)) {
        leftValue = arr.at(leftIndex);
    }

    if (!(rightIndex > arr.size() - 1)) {
        rightValue = arr.at(rightIndex);
    }

    //std::cout << "\tsiftDown(): (" << index << ") parent/left/right: " << indexValue << "/" << leftValue << "/" << rightValue << ", largest: ";

    if (indexValue > leftValue && indexValue > rightValue) {
       // std::cout << "Parent\n";
        return;

   } else if (leftValue >= rightValue && leftValue >= indexValue) {
       // std::cout << "left value\n";
        swapValues(index, leftIndex);
        siftDown(leftIndex);

    } else if (rightValue >= leftValue && rightValue >= indexValue) {
        //std::cout << "right value\n";
        swapValues(index, rightIndex);
        siftDown(rightIndex);
    }


  }


  void heapify () {
      for (int i = (arr.size() / 2) - 1; i >= 0; i--) {
          //std::cout << "Heapifying " << i << "\n";
          siftDown(i);

      }
  }

  void insert (int value) {
      arr.push_back(value);

      if (arr.size() == 1) {
          std::cout << "siftUp(): can't work on root index 0\n";
          //std::cout << "Root added\n";
          return;
      }
      //siftUp(arr.size() - 1);


  }



  void printTabs (int level) {
      for (int i = 0; i < level; i++) {
          std::cout << "\t";
      }

      std::cout << "|-------";

  }





  void printRecusive (int index, int level) {
      //std::cout << "printRecursive():: index/level: " << index << "/" << level << "\n";
      int leftIndex  = (index * 2) + 1;
      int rightIndex = leftIndex + 1;

    //std::cout << "\t left/right indices: " << leftIndex << "/" << rightIndex << "\n";

      if (!(leftIndex > arr.size() - 1)) {
        printRecusive(leftIndex, ++level);
        level--;
      }

    printTabs(level);
      std::cout << "(" << index << "): " << arr.at(index) << "\n";
      if (!(rightIndex > arr.size() - 1)) {
        printRecusive(rightIndex, ++level);
        level--;
      }


  }


  void print () {
      printRecusive(0, 0);
  }



};


int main()

{
    BinaryHeap heap;

    std::cout << "\nInserting numbers..\n=====================\n";
    srand (1);

    for (int i = 0; i < 18; i++) {
        int num = rand() % 20 + 1;
        heap.insert(num);
    }


    std::cout << "\nHeap as is..\n=====================\n";
    heap.print();

    std::cout << "\nHeapified..\n=====================\n";
    heap.heapify();
    heap.print();
    std::cout << "\nFinished.\n";

}
