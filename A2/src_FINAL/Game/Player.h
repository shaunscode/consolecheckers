/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "../App/common.h"


class Player {

private:

	//compPlayer flag is to determine if the AI should handle making a move
	//it's also saved to file for saved game purposes
	//GameController handles getting input, and will use this flag to determine if
	//AI returns some input, or program gets input from the console from a human player
	bool compPlayer;
	std::unique_ptr<std::string> name;
	int id;
protected:
public:

	Player ():
		compPlayer(false),
		name(nullptr),
		id(-1)	 {
		Debug::info("Player constructed.\n");
	}
/*
	Player (Player& inPlayer):
		name(std::string(*inPlayer->getName()),
		id(inPlayer->getId()) {
		
	}
*/
	~Player () {
		Debug::info("Player deconstructed.\n");
	}
	void setName (std::string inName) {
		name = std::make_unique<std::string>(inName);
	}

	void setId (int inId) {
		id = inId;
	}

	int getId () {
		return id;
	}

	std::string* getName () {
		return name.get();
	}

	void setCompPlayer (int isComp) {
		compPlayer = isComp;
	}

	bool isCompPlayer () {
		return compPlayer;
	}

};
