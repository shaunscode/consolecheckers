/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *
 */

#pragma once
#include <sstream>
#include "./GamePosition.h"

class GameMove {

private:
	GamePosition from;
	GamePosition to;
protected:
public:

	GameMove (GamePosition& inFrom, GamePosition& inTo):
		from(inFrom),
		to(inTo) {
	}

	GameMove (GameMove& inMove):
		from(inMove.getFrom()),
		to(inMove.getTo()) {
	}

	~GameMove () {

	};

	GamePosition& getTo () {
		return to;
	}

	GamePosition& getFrom () {
		return from;
	}

	std::string toString () {
		std::stringstream ss;
		//adding + 1 here to all values are in GameMove obj the GamePosition obj's are storing 
		//interal array coods, but the 'input' for the AI to call has to be 
		//in user coords to be process properly.

		ss << (from.getCol() + 1) << "," << (from.getRow() + 1) << "-" << (to.getCol() + 1) << "," << (to.getRow() + 1);
		std::string retString = ss.str();
//		std::cout << "GameMove::toString(): returning " << retString << "\n";
		return ss.str();
	}
};
