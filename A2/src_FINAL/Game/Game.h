/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once
#include <array>
#include "../Model/GameModel.h"

const int GAME_BOARD_SIZE = 8;

typedef std::array<std::array<int, GAME_BOARD_SIZE>, GAME_BOARD_SIZE> GameBoard;


enum class PieceType {
	NONE = 0,
	RED = 1, 
	WHITE = 2,
	RED_KING = 3,
	WHITE_KING = 4,
	INVALID = -1
};


class Player;
class GamePosition;

class Game {

private:

	GameState state;
	int redPlayerId;
	int whitePlayerId;
	int redPlayerScore;
	int whitePlayerScore;
	time_t saveTime;
	std::unique_ptr<GameBoard> board;

protected:
public:

	Game ();
	Game (Game* inGame);
	~Game ();

	void initGamePieces ();
	
	void setRedPlayerId (int inPlayer);
	int getRedPlayerId ();

	void setWhitePlayerId (int inPlayer);
	int getWhitePlayerId ();

	void incRedPlayerScore ();
	void setRedPlayerScore (int inScore);
	int getRedPlayerScore ();

	void incWhitePlayerScore ();
	void setWhitePlayerScore (int inScore);
	int getWhitePlayerScore ();

	void setGameState (GameState inGameState);
	GameState getGameState ();

	void setSaveTime (time_t inTime);
	time_t getSaveTime ();

	void copyGameBoard(GameBoard* inBoard);
	GameBoard* getGameBoard ();
	void setGameBoard (std::unique_ptr<GameBoard> inBoard);

	std::unique_ptr<std::vector<GamePosition>> getRedPieces ();
	std::unique_ptr<std::vector<GamePosition>> getWhitePieces ();
};
