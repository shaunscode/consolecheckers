/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseGameCmd.h"
#include "../../Game/Game.h"

class GameDoLoad: public BaseGameCmd {
private:


protected:
public:

	//without constructor passig it the controller up,
	//we get an error trying to convert the controller to a cmd
	//- attempting to use a default copy constructor??
	GameDoLoad (GameController& inController):
		BaseGameCmd(inController) {
	}

	//call game controller's do load game method
	//it'll call the app's load menu 
	//set menu state to LOAD
	void execute () {
		game.doLoadGame();
	}



};
