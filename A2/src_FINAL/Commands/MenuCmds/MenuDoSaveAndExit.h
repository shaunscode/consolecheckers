/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseMenuCmd.h"

class MenuDoSaveAndExit: public BaseMenuCmd {
private:
protected:
public:

	//without constructor passig it the controller up,
	//we get an error trying to convert the controller to a cmd
	//- attempting to use a default copy constructor??
	MenuDoSaveAndExit (MenuController& inController):
		BaseMenuCmd(inController) {
	}

	void execute () {
		menu.doSaveAndExit();
	}



};
