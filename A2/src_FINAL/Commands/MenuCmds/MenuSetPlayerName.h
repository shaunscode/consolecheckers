/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseMenuCmd.h"

class MenuSetPlayerName: public BaseMenuCmd {
private:
protected:
public:

	MenuSetPlayerName (MenuController& inController):
		BaseMenuCmd(inController) {
	}

	void execute () {
//		Debug::success("MenuSetPlayerName::execute() is being called!");
		menu.setPlayerName(*getData());
	}



};
