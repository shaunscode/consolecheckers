/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseMenuCmd.h"

class MenuSetPlayer: public BaseMenuCmd {
private:

	int playerIndex;

protected:
public:

	MenuSetPlayer (MenuController& inController):
		BaseMenuCmd(inController),
		playerIndex(-1) {
	}

	void execute () {
//		Debug::success("MenuSetPlayer::execute() is being called!");
		menu.setPlayer(*getData());
	}

};
