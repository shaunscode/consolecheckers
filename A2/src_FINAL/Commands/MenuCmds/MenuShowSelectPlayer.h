/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018
 *
 *	This cmd shows the player selection screen after
 *	PLAY has been selected from the Welcome screen
 */
#pragma once

#include "./BaseMenuCmd.h"

class MenuShowSelectPlayer: public BaseMenuCmd {
private:
protected:
public:

	MenuShowSelectPlayer (MenuController& inController):
		BaseMenuCmd(inController) {

	}

	void execute () {
		Debug::success("MenuShowSelectPlayer::execute() is being called!\n");
		menu.showSelectPlayer();
	}



};
