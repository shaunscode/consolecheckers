/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "../BaseCmd.h"
#include "../../Controller/MenuController.h"

class MenuController;

class BaseMenuCmd: public BaseCmd {
private:
protected:
	MenuController& menu;
public:

	BaseMenuCmd (MenuController& inMenu):
		menu(inMenu) {
	}
	
	virtual ~BaseMenuCmd () {};

};
