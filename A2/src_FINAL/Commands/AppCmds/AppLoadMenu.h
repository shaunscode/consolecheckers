/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseAppCmd.h"

class AppLoadMenu: public BaseAppCmd {
private:
	
	MenuState thisState;

protected:
public:

	AppLoadMenu(App& inApp):
		BaseAppCmd(inApp),
		thisState(WELCOME) {
	}

	void execute () {
		app.loadMenu(thisState);
	}
	
	void setMenuState (MenuState inMenuState) {
		thisState = inMenuState;
	}

};
