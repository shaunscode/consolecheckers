/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseAppCmd.h"

class AppSaveAndQuit: public BaseAppCmd {
private:
protected:
public:

	AppSaveAndQuit(App& inApp):
		BaseAppCmd(inApp) {
	}

	void execute () {
		app.saveAndQuit();
	}

};
