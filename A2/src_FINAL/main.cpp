/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#include "./Debug.h"

#include "./App/App.h"
#include "./App/IO.h"
#include <string>
#include <iostream>
#include <time.h>


void printUsage () {
	IO::print("\nEnglish Draughts Help\n", WHITE);
	IO::print("======================\n", BLUE);
	IO::print("Usage: ./draughts ", WHITE);
	IO::print("<", RED);
	IO::print("filename", WHITE);
	IO::print(">\n", RED);
	IO::print("\nWhere ", WHITE);
	IO::print("<", RED);
	IO::print("filename", WHITE);
	IO::print("> ", RED);
	IO::print("is the name of the file used to store your player and saved games data.\n\n", WHITE);
	
}


int main(const int argc, const char** argv) {





/*
	Debug::toggleInfo();
	Debug::toggleWarning();
	Debug::toggleError();
*/
	//cmd argvs should only be the filename or --help
	if (argc != 2) {
		printUsage();
		return 1;
	}

	std::string filename = std::string(argv[1]);

	if (filename.compare("--help") == 0) {
		printUsage();
		return 1;
	}
	
	//set randomd seed for ANY random call in the app
	srand (time(NULL));

	App app(filename);

}
