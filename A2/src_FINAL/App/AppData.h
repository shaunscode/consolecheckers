/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *
 *	Data save/loaded to the file
 *	BaseController is populated with this for updating the
 *	players and games data
 *

 *
 *
 *
 *
 */

#pragma once

#include <vector>

#include <memory>



enum DataState {
	MODIFIED,
	SAVED
};

class Player;
class Game;

class AppData {

private:

	DataState state;
	std::unique_ptr<std::vector<std::unique_ptr<Player>>> players;
	std::unique_ptr<std::vector<std::unique_ptr<Game>>> games;

protected:

public:

	AppData ():
		state(MODIFIED),
		players(nullptr),
		games(nullptr) {	//create as modified as if 'empty'

	}
	~AppData () {

	}
	
	std::vector<std::unique_ptr<Player>>* getPlayers ();
	std::vector<std::unique_ptr<Game>>* getGames ();
	void setPlayers (std::unique_ptr<std::vector<std::unique_ptr<Player>>> inPlayers);
	void setGames (std::unique_ptr<std::vector<std::unique_ptr<Game>>> inGames);

	//util methods for getting specific player/game
	Player* getPlayer (int inPlayerId);
	Game* getGame (int inGameId);

	void addPlayer (std::unique_ptr<Player> inPlayer);
	void addGame (std::unique_ptr<Game> inGame);
	
		





};
