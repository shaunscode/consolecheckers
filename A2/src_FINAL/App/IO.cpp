/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */
#include "../Debug.h"
#include "./IO.h"
#include "./AppData.h"
#include "./FileIO.h"
#include "../Game/Player.h"
#include "../Game/Game.h"

//prints a colour char to change the colour of the text that follows
void IO::setColor (enum color inColor) {
		switch (inColor) {
			case (CYAN):
				std::cout << "\033[36m";
				break;
			case (MAGENTA):
				std::cout << "\033[35m";
				break;
			case (RED):
				std::cout << "\033[31m";
				break;
			case (GREEN):
				std::cout << "\033[32m";
				break;
			case (BLUE):
				std::cout << "\033[34m";
				break;
			case (YELLOW):
				std::cout << "\033[33m";
				break;
			default:	//WHITE
				std::cout << "\033[37m";
		}
	}


 	 void IO::print (int inInt) {
 		 IO::print(std::to_string(inInt));
 	 }

 	 void IO::print (int inInt, enum color inColor) {
 		 IO::print(std::to_string(inInt), inColor);
 	 }

	void IO::print (std::string inString) {
		IO::setColor(WHITE);
		std::cout << inString;
	}

	void IO::print (std::string inString, enum color inColor) {
		IO::setColor(inColor);
		std::cout << inString;
	}

	std::string IO::getInput () {

		char buffer[50];

		IO::print("\nINPUT");
		IO::print("> ", GREEN);
		IO::setColor(WHITE);
		std::cin.getline(buffer, sizeof(buffer));
				
		return std::string(buffer);
	}


	std::unique_ptr<Player> getCompPlayer (std::string inName, int inId) {
		std::unique_ptr<Player> compPlayer;
		compPlayer = std::make_unique<Player>();
		compPlayer->setCompPlayer(true);
		compPlayer->setName(inName);
		compPlayer->setId(inId);
		return std::move(compPlayer);
		
	}

	std::unique_ptr<AppData> IO::getEmptyAppData () {
		std::unique_ptr<AppData> appData = std::make_unique<AppData>();
		appData->setPlayers(std::make_unique<std::vector<std::unique_ptr<Player>>>());	
		appData->setGames(std::make_unique<std::vector<std::unique_ptr<Game>>>());	
		appData->addPlayer(getCompPlayer("Shaun (Computer)", 9999));
		appData->addPlayer(getCompPlayer("Alicia (Computer)", 9998));
		return std::move(appData);	
	}	


	std::unique_ptr<AppData> IO::loadAppData (std::string inFileName) {
		Debug::info("IO::loadAppData() called..\n");
		FileIO fileIO;
		std::unique_ptr<AppData> appData = fileIO.loadAppData(inFileName);
		if (appData == nullptr) {
			IO::error("Failed to load saved files.\n");
			appData = getEmptyAppData();		
		}
		return std::move(appData);
	}

	void IO::saveAppData (std::string inFileName, AppData* inAppData) {
		FileIO fileIO;
		fileIO.saveAppData(inFileName, inAppData);
	}


	 void IO::success(std::string inString) {
			print("SUCCESS: ", GREEN);
			print(inString, WHITE);

		}
	 void IO::note (std::string inString) {
		print("*** ", CYAN);
		print(inString, WHITE);
	}
	

	 void IO::info (std::string inString) {
		print("INFO: ", BLUE);
		print(inString, WHITE);
	}

	 void IO::warning (std::string inString) {
		print("** WARNING: ", YELLOW);
		print(inString, WHITE);
	}

	 void IO::error (std::string inString) {
		print("### ERROR: ", RED);
		print(inString, WHITE);
	}

