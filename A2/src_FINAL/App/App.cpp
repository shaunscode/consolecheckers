/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#include <map>

#include "./App.h"

#include "../Model/MenuModel.h"
#include "../View/MenuView.h"
#include "../Controller/MenuController.h"
#include "../Input/MenuInput.h"


#include "../Model/GameModel.h"
#include "../View/GameView.h"
#include "../Controller/GameController.h"
#include "../Input/GameInput.h"

#include "../Game/Player.h"
#include "../Game/Game.h"

#include "../Model/MenuModel.h"

//#include "../Controller/BaseController.h"
//#include "../Model/DataModel.h"
//#include "../Controller/GameController.h"
//#include "../Controller/SaveController.h"
//#include "../Controller/LoadController.h"

//#include "../View/BaseView.h"
//#include "../View/GameView.h"
//#include "../View/SaveView.h"
//#include "../View/LoadView.h"


BaseController* App::getController (ControllerEnum inControllerEnum) {
	auto set = MVCSets.find(inControllerEnum);

	if (set == MVCSets.end()) {
		return controller;	//returns the nullptr
	}
	return set->second.get();
}

void App::initMVC () {
//	Debug::info("initMVC() called..\n");

	//Menu MVC setup
	std::unique_ptr<MenuController> menuController;
	menuController = std::make_unique<MenuController>(*this);	//deref 'this' pointer
	menuController->setModel(std::make_unique<MenuModel>(*appData));
	menuController->setView(std::make_unique<MenuView>());
	menuController->setInput(std::make_unique<MenuInput>()); 
	menuController->buildInputMaps();
	MVCSets.insert(std::make_pair(MENU, std::move(menuController)));

	//Game MVC setup
	std::unique_ptr<GameController> gameController;
	gameController = std::make_unique<GameController>(*this);	//deref 'this' pointer
	gameController->setModel(std::make_unique<GameModel>(*appData));
	gameController->setView(std::make_unique<GameView>());
	gameController->setInput(std::make_unique<GameInput>());
	gameController->buildGameCmds();
	MVCSets.insert(std::make_pair(GAME, std::move(gameController)));


}

void App::initApp () {
//	Debug::info("initApp() called..\n");
	controller = getController(MENU);
	if (controller == nullptr) {
		Debug::error("App::iniApp(): controller is nullptr!\n");
	}

}
void App::loadAppData (std::string inFileName) {
	appData = IO::loadAppData(inFileName);
}

void App::saveAppData () {
	try {
		IO::saveAppData(saveFileName, appData.get());
	} catch (...) {
		Debug::error("Exception caught while saving data..\n");
	}
	Debug::success("AppData has been saved!\n");
}

void App::handleAppCmd (std::unique_ptr<BaseAppCmd> inAppCmd) {
	if (inAppCmd == nullptr) {
//		Debug::warning("App::handleAppCmd(): command was nullptr!\n");
		return;
	}
	inAppCmd->execute();
	inAppCmd.reset();	//clear memory
};

void App::mainLoop () {
//	Debug::info("mainLoop() called..\n");
	running = true;
	int i = 0;
	while (running && i < 9999) {
//		Debug::info("\tin main wile loop..\n");
		handleAppCmd(controller->update());
		i++;
	}
}




void App::quit () {
//	Debug::info("quit() called\n");
	running = false;
}

void App::saveAndQuit () {
	saveAppData();
	quit();
}
void App::loadMenu (MenuState& inMenuState) {
//	Debug::info("loadMenu() called..\n");


	MenuController* menuController;

	if (inMenuState == LOAD) {
		MenuController* menuController = dynamic_cast<MenuController*>(getController(MENU));
		menuController->showLoad();
		controller = menuController;
	} else {
		controller = getController(MENU);
	}
}

void App::loadGame (std::unique_ptr<Game> inGame) {
//	Debug::info("loadGame() called..\n");
	controller = getController(GAME);
	GameController* gameController = dynamic_cast<GameController*>(controller);
	gameController->setGame(std::move(inGame));

}

void App::saveGame (std::unique_ptr<Game> inGame) {
	Debug::info("App::saveGame(): Writing game to app data and saving\n");
	appData->addGame(std::move(inGame));
	saveAppData();
}
