cmake_minimum_required(VERSION 2.8.12.2)

project(checkers)

set(SRC main.cpp Debug.h)

file(GLOB APP App/*.h App/*.cpp)
file(GLOB COMMANDS Commands/*.h Commands/*.cpp)
file(GLOB CONTROLLERS Controller/*.h Controller/*.cpp)
file(GLOB GAME Game/*.h Game/*.cpp)
file(GLOB INPUT Input/*.h Input/*.cpp)
file(GLOB MODEL Model/*.h Model/*.cpp)
file(GLOB VIEW View/*.h View/*.cpp)

set(CMAKE_BUILD_TYPE Debug)

add_executable(run ${SRC} ${APP} ${COMMANDS} ${CONTROLLERS} ${GAME} ${INPUT} ${MODELS} ${VIEWS})
