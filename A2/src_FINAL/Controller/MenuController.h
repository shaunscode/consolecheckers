/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseController.h"

class App;

class MenuController: public BaseController {

private:
	Game* getGame ();
	std::unique_ptr<Game> getCopyOfGame (Game* inGame);
protected:

	void updateModel ();
	bool isNameValid (std::string inName);

public:

	 MenuController (App& inApp):
		BaseController(inApp) {
	//	Debug::info("MenuController constructed.\n");
	 }

	~MenuController () {
	//	Debug::info("MenuController deconstructed.\n");
	}



	void showWelcome();
	void showAddPlayer ();
	void showSelectPlayer ();
	void setPlayerName (std::string inName);
	void setPlayer (std::string inIndex);
	void showLoad ();
	void showLoadSuccess ();
	void doLoadGame (int inGameIndex);

	void doPlayGame (std::unique_ptr<Game> inGame);
	void doSaveAndExit ();
	void doExit ();

	int getNewPlayerId ();
	void addNewPlayer (std::string inName);
	int getNewPlayeId ();
	void buildInputMaps ();
};
