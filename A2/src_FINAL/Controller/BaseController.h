/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "../App/App.h"

//forward declaration
class App;
class BaseAppCmd;
class BaseInput;
class BaseModel;
class BaseView;

//template<class View, class Mode>
class BaseController {
private:
protected:
	//reference to App driver
	App& app;

	//controllers own the memeory for the following
	std::unique_ptr<BaseAppCmd> nextCmd;
	std::unique_ptr<BaseModel> model;
	std::unique_ptr<BaseView> view;
	std::unique_ptr<BaseInput> input;

	std::unique_ptr<BaseAppCmd> getNextAppCmd ();
	void setNextAppCmd (std::unique_ptr<BaseAppCmd> inNextCmd);
	virtual void handleCmd(BaseCmd* inCmd);

	//these methods are for overriding if necessary
	virtual void updateModel ();
	virtual void updateView ();
	virtual void updateController ();
	virtual std::string getInput ();

public:

	BaseController (App& inApp):
		app(inApp) {
//		Debug::info("BaseController constructed.\n");
	}
	
	virtual ~BaseController () {
//		Debug::info("BaseController deconstructed.\n");
	}


	//renders screen according to the model
	std::unique_ptr<BaseAppCmd> update () ;
	void setModel (std::unique_ptr<BaseModel> inModel);
	void setView (std::unique_ptr<BaseView> inView);
	void setInput (std::unique_ptr<BaseInput> inInput);

	BaseModel* getModel () {
		return model.get();
	}

};
