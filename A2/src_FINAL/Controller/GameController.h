/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *
 */

#pragma once

#include "./BaseController.h"
#include "../Game/Game.h"
#include "../Game/Ai.h"

class App;
class GameMove;

class GameController: public BaseController {

private:
	std::unique_ptr<Ai> ai;
	
	bool isPosEqualPiece (int inX, int inY, const PieceType& inPiece);
	bool isPosEqualPiece (GamePosition& inPos, const PieceType& inPiece);
	void getValidForwardPositions (GamePosition& inPos, int inDir, GameState& inGameState, std::vector<GamePosition>& retPoss);
	void getValidBackPositions (GamePosition& inPos, int inDir, GameState& inGameState, std::vector<GamePosition>& retPoss);
	bool isPosDeltaValid (GamePosition inPos, int inRowsAhead, int inColsSided);

	bool isPosEmpty (GamePosition& inPos);
	bool isPosEnemy (GamePosition& inPos, GameState& inState);

	
	bool isGameEnd(Game& inGame);

protected:

	//on succesful move, this is called to swap player turns state
	//in the model
	void swapPlayerTurns ();
	
	void checkPieceToKing (int inRow, int inCol);
		
	//checks if the position being moved too is empty or not
	bool isMoveToPositionAvailable(GameMove& inMove);

	//checks the move to->from piece is two spots away and 
	//if and enemy piece is in between
	bool isMoveCapturingPiece(GameState& gameState, GameMove& inMove);
	//checks to->from distance is only one square AND in the correct
	//direction of the curret player
	bool isMoveWithinLimits(GameMove& inMove);
	
	//ensure the TO position actually contains the current players
	//piece
	bool isPosPlayers(GameState& gameState, GamePosition& inPos);
	//main method to validate the move, passing specific validations
	//to the other helper methods above
	bool isMoveValid (GameMove& inMove, GameState& inGameState);


	//OVERRIDE of the BaseController::getInput() method
	//want to handle input via the controller so we can pull input
	//from the Computer players too without always going to console
	//to get input or going via the GameInput to get the ComputerPlayer
	//input, as ComputerPlayer will be owned by the GameController
	std::string getInput ();


public:

	 GameController (App& inApp):
		BaseController(inApp),
		ai(nullptr) {
		Debug::info("GameController constructed.\n");
		ai = std::make_unique<Ai>(*this);
	 }

	~GameController () {
		Debug::info("GameController deconstructed.\n");
	}

	//consume game data for playing game
	void setGame (std::unique_ptr<Game> inGame);

	void getValidPositions (GamePosition& inPos, GameState& inGameState, std::vector<GamePosition>& retPoss);
	bool isPosWithinBoard(GamePosition& inPos);
	//used for determining whether to forward the getInput() for a game move
	//too the comp AI
	bool isPlayerComp (int inPlayerId);
	bool isCompTurn (GameModel& gameModel);

	void doMove (GameMove* inMove);
	void doSaveGame (Game* inGame);
	void doLoadGame ();
	
	void buildGameCmds ();
};
