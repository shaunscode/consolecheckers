#include "../Debug.h"
#include "./AppData.h"
#include "../Game/Player.h"
#include "../Game/Game.h"

	std::vector<std::unique_ptr<Player>>* AppData::getPlayers () {
		return players.get();
	}

	std::vector<std::unique_ptr<Game>>* AppData::getGames () {
		return games.get();
	}

	void AppData::setPlayers (std::unique_ptr<std::vector<std::unique_ptr<Player>>> inPlayers) {
		players = std::move(inPlayers);
	}

	void AppData::setGames (std::unique_ptr<std::vector<std::unique_ptr<Game>>> inGames) {
		games = std::move(inGames);
	}

	//util methods for getting specific player/game
	Player* AppData::getPlayer (int inPlayerId) {
		Debug::todo("AppData::getPlayer(): return player by id");
		return nullptr;
	}

	Game* AppData::getGame (int inGameId) {
		Debug::todo("AppData::getGame(): return game by id");
		return nullptr;
	}

	//convenience methods to add a player/game to data
	void AppData::addPlayer (std::unique_ptr<Player> inPlayer) {
		players->push_back(std::move(inPlayer));
	}
	
	void AppData::addGame (std::unique_ptr<Game> inGame) {
		games->push_back(std::move(inGame));
	}
	
