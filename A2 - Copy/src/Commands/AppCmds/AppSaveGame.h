/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include <memory>

#include "./BaseAppCmd.h"
#include "../../Game/Game.h"

class AppSaveGame: public BaseAppCmd {
private:
	std::unique_ptr<Game> game;
protected:
public:

	AppSaveGame(App& inApp):
		BaseAppCmd(inApp),
		game(nullptr) {
	}

	void execute () {
		//moving game data from Menu to Game modules, have to move the memory
		Debug::info("AppSaveGame::execute(): Saving the game:\n");
		std::cout << "\tRedPlayerID: " << (game->getRedPlayerId()) << "\n";
		std::cout << "\tWhitePlayerID: " << (game->getWhitePlayerId()) << "\n";

		app.saveGame(std::move(game));
	}

	void setGame(std::unique_ptr<Game> inGame) {
		game = std::move(inGame);
	}

};
