/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseGameCmd.h"
#include "../../Game/Game.h"

class GameDoSave: public BaseGameCmd {
private:


protected:
public:

	//without constructor passig it the controller up,
	//we get an error trying to convert the controller to a cmd
	//- attempting to use a default copy constructor??
	GameDoSave (GameController& inController):
		BaseGameCmd(inController) {
	}

	//BaseGameCommand has a ref to the contrloller.
	//just pull the game state from teh controler->model->game
	void execute () {
		GameModel* gameModel = dynamic_cast<GameModel*>(game.getModel());
		//copy of state to save.
		Game* saveGame = gameModel->getGame();
		game.doSaveGame(saveGame);
	}



};
