/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "../BaseCmd.h"
#include "../../Controller/GameController.h"

class GameController;

class BaseGameCmd: public BaseCmd {
private:
protected:
	GameController& game;
public:

	BaseGameCmd (GameController& inGame):
		game(inGame) {
	}

	virtual ~BaseGameCmd () {};

};
