/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseGameCmd.h"
#include "../../Game/GameMove.h"

class GameDoMove: public BaseGameCmd {
private:
	std::unique_ptr<GameMove> gameMove;

protected:
public:

	//without constructor passig it the controller up,
	//we get an error trying to convert the controller to a cmd
	//- attempting to use a default copy constructor??
	GameDoMove (GameController& inController):
		BaseGameCmd(inController),
		gameMove(nullptr) {
	}

	void execute () {
		game.doMove(getGame());
	}

	void setMove (std::unique_ptr<GameMove> inMove) {
		gameMove = std::move(inMove);
	}

	GameMove* getGame () {
		return gameMove.get();
	}


};
