/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include <sstream>
#include <memory>

class BaseCmd {
private:
	std::unique_ptr<std::string> dataString;

protected:
public:
	virtual ~BaseCmd () {};

	virtual void execute () = 0;

	virtual void setData (std::string inDataString) {
		dataString = std::make_unique<std::string>(inDataString);
	}

	virtual std::string* getData () {
		return dataString.get();
	}

	virtual int getDataAsInt () {
		std::stringstream ss(*dataString.get());
		int i = 0;
		ss >> i;
		return i;
	}

};
