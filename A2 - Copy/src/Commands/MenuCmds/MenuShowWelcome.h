/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseMenuCmd.h"

class MenuShowWelcome: public BaseMenuCmd {
private:
protected:
public:

	MenuShowWelcome (MenuController& inController): 
		BaseMenuCmd(inController) {
	}

	void execute () {
		Debug::success("MenuShowWelcomer::execute() is being called!\n");
		menu.showWelcome();
	}



};
