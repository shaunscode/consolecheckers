/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseMenuCmd.h"

class MenuLoadGame: public BaseMenuCmd {
private:

	int gameIndex;

protected:
public:

	MenuLoadGame (MenuController& inController):
		BaseMenuCmd(inController),
		gameIndex(-1) {
	}

	void execute () {
//		Debug::success("MenuLoadGame::execute() is being called!");
		menu.doLoadGame(getDataAsInt());
	}

	void setPlayerIndex (int inIndex) {
		gameIndex = inIndex;
	}

	int getGameIndex () {
		return gameIndex;
	}

};
