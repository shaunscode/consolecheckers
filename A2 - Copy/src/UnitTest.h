/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include <iostream>
#include <string>

#include "./App/IO.h"
#include "./View/MenuView.h"

class UnitTest {
private:
protected:
public:
	static void testAll () {

	}

	static void testMenuView () {
		MenuView menu;
		menu.showMain();
		menu.showEnterName();
		menu.showNameValid("UnitTestName");
		menu.showNameInvalid("UnitTestName");
		menu.showMove();
		menu.showLoad();
		menu.showSave();
	}

};
