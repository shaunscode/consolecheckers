/*
*	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#pragma once

#include "./BaseModel.h"


//seperate to MenuModel class so other classes can use it
enum MenuState {
	WELCOME,
	ADD_PLAYER,
	NAME_VALID,
	NAME_INVALID,
	SELECT_PLAYER,
	LOAD,
	LOAD_SUCCESS,
	SAVE_AND_EXIT,
	EXIT
};


class AppData;
class Game;

class MenuModel: public BaseModel {

private:
	std::unique_ptr<Game> gameData;
protected:
public:

	MenuModel (AppData& inAppData):
		BaseModel(inAppData) {
		
	//	Debug::info("MenuModel constructed.\n");
	}

	~MenuModel () {
	//	Debug::info("MenuModel deconstructed.\n");
	}

	//sets state, only accepts MenuStates
	//outside classes can see this
	//it then sets the protected BaseModel modelState value
	void setState (MenuState& inState) {
		modelState = inState;
	}

	void setGame (std::unique_ptr<Game> inGame) {
		gameData = std::move(inGame);
	}

	std::unique_ptr<Game> getNewGame () {
		Debug::info("MenuModel::getNewGame(): called..\n");
		std::unique_ptr<Game> game;
		game = std::make_unique<Game>();
		return std::move(game);
	}	

	//gets the temp game data being created via the menu
	//if there is none, it will make an empty one
	Game* getGame () {
		if (gameData == nullptr) {
			//no game? create one and get it back
			gameData = std::move(getNewGame());
		}
		return gameData.get();
	}

	std::unique_ptr<Game> takeGame () {
		return std::move(gameData);
	}


	


	



};
