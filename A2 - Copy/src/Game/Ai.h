/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *
 */

#pragma once

#include <memory>
#include "../Model/GameModel.h"


class Game;
class GameMove;
class GameController;
class GamePosition;
class GameMove;


class Ai {

private:
	void getMovesForPosition (GamePosition& inPos, GameState& inGameState, std::vector<std::unique_ptr<GameMove>>& retMoves);
	void getMovesForPosition (GamePosition& inPos, GameState& inGameState, std::vector<GameMove>& retMoves);
	void getMoves(Game& inGame, std::vector<std::unique_ptr<GameMove>>& retMoves);
	GameController& gameController;
	std::unique_ptr<GameMove> getMove (Game& inGame);
protected:
public:
	Ai (GameController& inGameController);
	~Ai ();
	std::string getMoveStr (Game& inGame);

};
