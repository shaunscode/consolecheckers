/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *
 */

#include "./Ai.h"
#include "./Game.h"
#include "./GameMove.h"
#include "../Controller/GameController.h"
#include <time.h>

#include <unistd.h>	

Ai::Ai (GameController& inController): 
	gameController(inController) {

}

Ai::~Ai () {

}

void Ai::getMovesForPosition (GamePosition& inPos, GameState& inGameState, std::vector<std::unique_ptr<GameMove>>& retMoves) {
	std::vector<GamePosition> validMovePositions;
	//pass off to get valid positions for the inPos var
	gameController.getValidPositions (inPos, inGameState, validMovePositions);

	//for all the valid positions found for inPos, create a GameMove obj out of them
	//and place it in the retting moves vec
	for (auto iter = validMovePositions.begin(); iter != validMovePositions.end(); iter++) {
		std::cout << "\trow/cols, from: " << inPos.getRow() << "/" << inPos.getCol() << "-" << (*iter).getRow() << "/" << (*iter).getCol() << "\n";
		retMoves.emplace_back(std::make_unique<GameMove>(inPos, *iter));
	}
}

void Ai::getMoves(Game& inGame, std::vector<std::unique_ptr<GameMove>>& retMoves) {
	//determine whos turn it is
	GameState gameState = inGame.getGameState();
	std::unique_ptr<std::vector<GamePosition>> pieces;

	//get all their piececs
	if (gameState == RED_TURN) {
		pieces = inGame.getRedPieces();
	} else if (gameState == WHITE_TURN) {
		pieces= inGame.getWhitePieces();
	} else {
		return;
	}

	//go over all piecesand get all available moves	
	for (auto iter = pieces->begin(); iter != pieces->end(); iter++) {
		getMovesForPosition(*iter, gameState, retMoves);
	}
}


std::string Ai::getMoveStr (Game& inGame) {
	return getMove(inGame)->toString();
}

std::unique_ptr<GameMove> Ai::getMove (Game& inGame) {
//	Debug::info("AI::getMove() called..\n");

	std::vector<std::unique_ptr<GameMove>>moves;
	getMoves(inGame, moves);
	
	//small sleep time for the thread so 
	//we can see what the comps are doing
	unsigned int microseconds = 10;
	usleep(microseconds);	

	//pick a random move, implemente tree later!
	int randomIndex = rand() % moves.size();
	return std::move(moves.at(randomIndex));
}

