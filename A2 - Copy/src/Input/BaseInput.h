#pragma once

class BaseCmd;

class BaseInput {

private:
protected:
public:

	BaseInput () {
//		Debug::info("BaseInput constructed\n");
	}
	
	virtual ~BaseInput () {
//		Debug::info("BaseInput deconstructed.\n");
	}
	
	virtual BaseCmd* handleInput (int inState, std::string inInput) = 0;

};
