#pragma once


#include <memory>

#include "./BaseInput.h"

class GameMove;
class GameDoSave;
class GameDoMove;
class GameDoLoad;
class GamePosition;
class GameInput: public BaseInput {

private:

	std::unique_ptr<GameDoMove> doMoveCmd;
	std::unique_ptr<GameDoSave> doSaveCmd;
	std::unique_ptr<GameDoLoad> doLoadCmd;

	bool isInputValidQuickCheck (std::string inInput);
	bool isMoveValid (GameMove& inMove);
	bool isInputSave (std::string inInput);
	bool isInputLoad (std::string inInput);
	
	std::unique_ptr<GameMove> parseMoveString (std::string inInput);
	void getGamePosFromStrPos (std::string inStrPos, GamePosition& retGamePos);

protected:
public:

	GameInput ();
	~GameInput ();
	BaseCmd* handleInput (int inState, std::string inInput);
	void buildGameCmds (GameController& inGameController);
};
