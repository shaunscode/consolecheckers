#include <algorithm>
#include <regex>

#include "../Commands/GameCmds/GameDoMove.h"
#include "../Commands/GameCmds/GameDoSave.h"
#include "../Commands/GameCmds/GameDoLoad.h"
#include "./GameInput.h"
#include "../Debug.h"
#include "../Game/GameMove.h"
#include "../Game/GamePosition.h"

#include<boost/tokenizer.hpp>



typedef boost::tokenizer< boost::char_separator<char> > boostTok;

GameInput::GameInput ():
	doMoveCmd(nullptr),
	doSaveCmd(nullptr),
	doLoadCmd(nullptr) {
	Debug::info("GameInput constructed\n");
}

GameInput::~GameInput () {
	Debug::info("BaseInput deconstructed.\n");
}

//This validation is purely to ensure the input and coords
//are valid, not that actual move that takes place.
//This is left to the GameController to determine. 
bool GameInput::isInputValidQuickCheck (std::string inInput) {
	//should be 7 chars long
	//X,Y-X,Y
	//todo: remove all spaces from input prior to this
	//calc to avoid falses on X,Y - X, Y and the like.
	if (inInput.size() != 7) {
		return false;
	}
	
	//should contain one - char (dash)
	size_t dash = std::count(inInput.begin(), inInput.end(), '-');
	if (dash != 1) {
		return false;
	}
	
	//should contain 2 , chars (commas)
	size_t commas = std::count(inInput.begin(), inInput.end(), ',');	
	if (commas != 2) {
		return false;
	}

	//should haveggt four numbers?
	//TODO: verify it has four numbers.
	return true;
}

//this parses the input string and returns it as a game move
void GameInput::getGamePosFromStrPos (std::string inStrPos, GamePosition& retGamePos) {
//	Debug::info("GameInput::getGamePosFromStrPos called, passing in : ");
//	std::cout << inStrPos << "\n";

	//input str should be x1,y1 at this point
	//tokenize with the comma to get the numbers
	boost::char_separator<char> sep(",");
	boostTok tokens(inStrPos, sep);

	//should only be two tokens here. anymoreand it's screwed 
	//and will have to return a nullptr which will force user
	//to re-enter a move.
	//note: we take the Y pos first over the X pos as the array layout
	//will inturrpret the first number we give it as a the ROW, second
	//as the COL. 

	//People will enter the COL first (x asix) and ROW second (y axis)
	//with their input. These have to be swapped when passing to 
	//the game to use.
	boostTok::iterator iter = tokens.begin();
	int posCol = std::stoi((*iter).c_str());
	int posRow = std::stoi((*++iter).c_str());
	
//	Debug::info("GameInput::getGamePosFromStrPor: col/row int's: ");
//	std::cout << posCol << "/" << posRow << "\n";
	
	//if the numbers aren't valid (between 1 and 8) 
	//then the pos entered isn't valid and the reGamePos
	//x/y values will remain the initial -1
	//otherwise update te retGamePos x/ values
	if ((posCol >= 1 && posCol <= 8) && (posRow >= 1 && posRow <= 8)) {
		//we minus 1 here to bring user input coords into
		//alignments with board array indices, as user 
		//input 1-1 would really be 0-0 in the array.
		retGamePos.setRowCol(posRow - 1, posCol - 1);
	} 
}


//parses the input string into a GameMove object
//game mvoe object contains two positions (from and to)
//for the game to apply
std::unique_ptr<GameMove> GameInput::parseMoveString (std::string inInput) {

	//quick and dirty validaiton prior to parseing for GameMove's
	if (!isInputValidQuickCheck(inInput)) {
		return nullptr;
	}

	//seperate input at '-' to get the from and to strings
	boost::char_separator<char> sep("-");
	boostTok tokens(inInput, sep);

	//should only be two tokens here. anymoreand it's screwed 
	//and will have to return a nullptr which will force user
	//to re-enter a move.
	boostTok::iterator iter = tokens.begin();
	std::string fromStr = *iter;
	std::string toStr = *++iter;
	
	//some GamePosition vars to store the str->int conversions
	GamePosition fromPos;
	GamePosition toPos;

	//actually do the conversion, passes in a ref to our objs
	getGamePosFromStrPos (fromStr, fromPos);
	getGamePosFromStrPos (toStr, toPos);

		
	return std::make_unique<GameMove>(fromPos, toPos);	
}

bool GameInput::isInputSave (std::string inInput) {
	if (inInput.compare("s") == 0) {
		return true;
	}
	return false;
}



bool GameInput::isInputLoad (std::string inInput) {
	if (inInput.compare("l") == 0) {
		return true;
	}
	return false;
}



//Takes in the input string and makes something out of it.
//Just determines it is acceptable input for a controller
//command and passes it to the cmd controller.
BaseCmd* GameInput::handleInput (int inState, std::string inInput) {
	Debug::info("GameInput::handleInput called.\n");


	if (isInputSave(inInput)) {
		return doSaveCmd.get();
	}

	if (isInputLoad(inInput)) {
		return doLoadCmd.get();
	}

	std::unique_ptr<GameMove> gameMove;
	gameMove = parseMoveString(inInput);

	if (gameMove == nullptr) {
		return nullptr;
	}

	doMoveCmd->setMove(std::move(gameMove));
	return doMoveCmd.get();
}

//stores the commands for sending ot hte contorller here in the Input
//Input would really belong IN the controller, but its a bit large
//and has been seperated into it's own classes.
void GameInput::buildGameCmds (GameController& inGameController) {
	doMoveCmd = std::make_unique<GameDoMove>(inGameController);
	doSaveCmd = std::make_unique<GameDoSave>(inGameController);
	doLoadCmd = std::make_unique<GameDoLoad>(inGameController);
}
