/*
 *	Shaun Campbell (3666659)
 *	Programming Using C++ (004244)
 *	Assignment 2, SP1 2018 *
 *

 */

#include <memory>

#include "./GameController.h"

#include "../Game/Game.h"
#include "../Model/GameModel.h"
#include "../Game/GamePosition.h"
#include "../Game/GameMove.h"
#include "../Input/GameInput.h"
#include "../Commands/AppCmds/AppLoadMenu.h"
#include "../Commands/AppCmds/AppSaveGame.h"

void GameController::setGame (std::unique_ptr<Game> inGame) {
	GameModel* gameModel = dynamic_cast<GameModel*>(model.get());
	gameModel->setGame(std::move(inGame));
}

bool GameController::isPlayerComp (int inPlayerId) {
	Player* player = model->getPlayerById(inPlayerId);
	if (player->isCompPlayer()) {
		return true;
	}	
	return false;
}
	
bool GameController::isCompTurn(GameModel& gameModel) {
	Game* game = gameModel.getGame();
	GameState gameState = game->getGameState();
	if (
			(gameState == RED_TURN && isPlayerComp(game->getRedPlayerId())) ||
			(gameState == WHITE_TURN && isPlayerComp(game->getWhitePlayerId()))
	) {
		return true;
	}
	return false;

}


//this method is an override of the BaseController::getInput() method
//string returned from this will still go through the BaseController
//update() method that then passes it off to the Input::handleInput()
//method of the MVC Set.
//This is so we get intercept the game state and determine if it's a
//human or computer player, and direct the route to getting input to 
//either the AI object (computer) or the IO::getInput() from the 
//console (human). 
//The AI *should* return input in the same string format that a human
//would have to enter it x1,y1 - x2,y2.
//From this input, it's handle in GameInput::handleInput() method.

std::string GameController::getInput () {

//	Debug::info("GameController::getInput() called..\n");

	//get the game state from the GameModel
	GameModel& gameModel = dynamic_cast<GameModel&>(*model);


	if (isCompTurn(gameModel)) {
		Debug::success("getInput(): getting input from AI...");
		std::string move = ai->getMoveStr(*gameModel.getGame());
		std::cout << move << "\n";
		return move;
	} 

	//otherwise get humna input.
	return IO::getInput();
}

void GameController::buildGameCmds () {
	if (input == nullptr) {
		Debug::error("GameController::buildGameCmds():: there is no input to build for\n");
		return;
	}
	GameInput* gameInput = dynamic_cast<GameInput*>(input.get());
	gameInput->buildGameCmds(*this);	
}





bool GameController::isPosEqualPiece (GamePosition& inPos, const PieceType& inPiece) {
	return isPosEqualPiece(inPos.getRow(), inPos.getCol(), inPiece);
}

bool GameController::isPosEqualPiece (int inRow, int inCol, const PieceType& inPiece) {
//	Debug::info("GameController::isPosEqualPiece() called entered..");
//	std::cout << "Pos row/col: " << inRow << "/" << inCol << ", Piece: " << (int)inPiece << "\n";
	
	GameModel* gameModel = dynamic_cast<GameModel*>(model.get());
	GameBoard& board = *gameModel->getGame()->getGameBoard();
	
	//get the board piece and compare it to the
	//piece passed in
	int pieceType = board[inRow][inCol];
	if (pieceType == (int)inPiece) {
//		std::cout << ".. TRUE.\n";
		return true;
	}
//	std::cout << ".. FALSE.\n";
	return false;
}








bool GameController::isPosPlayers(GameState& gameState, GamePosition& inPos) {

//	Debug::info("GameController::isPlayerOwnerOfPiece() entered..\n");
	PieceType pieceType = PieceType::INVALID;
	PieceType pieceTypeKing = PieceType::INVALID;

	if (gameState == RED_TURN) {
		pieceType = PieceType::RED;
		pieceTypeKing = PieceType::RED_KING;
	} else if (gameState == WHITE_TURN) {
		pieceType = PieceType::WHITE;
		pieceTypeKing = PieceType::WHITE_KING;
	}


	if (isPosEqualPiece(inPos, pieceType) || isPosEqualPiece(inPos, pieceTypeKing)) {
		return true;
//		Debug::success("GameController::isPlayerOwnerOfPiece(): Player DOES own piece!\n");

	} 
//	Debug::error("GameController::isPlayerOwnerOfPiece(): Player does NOT own piece!\n");
	return false;

}


bool GameController::isPosEnemy (GamePosition& inPos, GameState& inState) {
	PieceType piece;
	PieceType pieceKing;

	if (inState == RED_TURN) {
		piece = PieceType::WHITE;
		pieceKing = PieceType::WHITE_KING;
	} else {
		piece = PieceType::RED;
		pieceKing = PieceType::RED_KING;
	}
	if (isPosEqualPiece(inPos, piece) || 
		isPosEqualPiece(inPos, pieceKing)) {
		return true;
	} 
	return false;


}


bool GameController::isMoveCapturingPiece(GameState& inGameState, GameMove& inMove){
//	Debug::info("GameController::isMoveCaputringPiece() entered..\n");
	int row = ((inMove.getTo().getRow() - inMove.getFrom().getRow()) / 2) + inMove.getFrom().getRow();
	int col = ((inMove.getTo().getCol() - inMove.getFrom().getCol()) / 2) + inMove.getFrom().getCol();
	GamePosition midPos(row, col);

	//in the event the to and from positions are 1,1/2,2 and 
	//the subtraction returns (1.5, 1.5), the GamePosition
	//vars are int's and with cut the decimal and return 1,1
	//anyhow, which will not be an enemy piece but the players
	//own piece
	if (isPosEnemy(midPos, inGameState)) {
//		Debug::info("GameController::isMoveCapturingPiece(): Pos IS an enemy!\n");
		return true;
	}

	Debug::info("GameController::isMoveCapturingPiece(): pos was NOT enemy\n");
	return false;
}


bool GameController::isPosEmpty (GamePosition& inPos) {
	if (isPosEqualPiece(inPos, PieceType::NONE)) {
		return true;
	}
	return false;
}

//just checks if pos is within the board size
bool GameController::isPosWithinBoard(GamePosition& inPos) {
	if (inPos.getCol() < 0 ||
		inPos.getCol() >= GAME_BOARD_SIZE ||
		inPos.getRow() < 0 ||
		inPos.getRow() >= GAME_BOARD_SIZE) {
	//	Debug::error("isPosWithintBoard(): pos wasn't within board!");
		std::cout << " row/col: " << inPos.getRow() << "/" << inPos.getCol() << "\n";
		return false;

	}
	return true;
}

//basically just takes in a position (presumabley the current position
//a piece) and gets the forward left/right positions of that piece
//and determines if they are free spots or within the board and
//adds them to the returning positions vector.
void GameController::getValidForwardPositions (GamePosition& inPos, int inDir, GameState& inGameState, std::vector<GamePosition>& retPoss) {
//	Debug::info("GameController::getValidFowardPositions() entered..\n");

	//get a GamePosition 'forward' and Left
	GamePosition fwdLeft(inPos.getRow() + inDir, inPos.getCol() - 1);

	if (isPosWithinBoard(fwdLeft)) {

		//is it empty and within the board?
		if (isPosEqualPiece(fwdLeft, PieceType::NONE)) {
			retPoss.emplace_back(fwdLeft);

		} else if (isPosEnemy(fwdLeft, inGameState)) {
			
			//get the position just over the enemy pos and see if it's empty/avaliable
			//make a GamePos for it
			GamePosition furtherFwdLeft(inPos.getRow() + (inDir * 2), inPos.getCol() - 2);
			if (isPosWithinBoard(furtherFwdLeft)) {

				if (isPosEqualPiece(furtherFwdLeft, PieceType::NONE)) {
					retPoss.emplace_back(furtherFwdLeft);
				}
			}
		}

	}


	//get a GamePosition 'forward' and right
	GamePosition fwdRight(inPos.getRow() + inDir, inPos.getCol() + 1);

	if (isPosWithinBoard(fwdRight)) {

		//is it empty and within the board?
		if (isPosEqualPiece(fwdRight, PieceType::NONE)) {
			retPoss.emplace_back(fwdRight);

		} else if (isPosEnemy(fwdRight, inGameState)) {

			//get the position just over the enemy pos and see if it's empty/avaliable
			//make a GamePos for it
			GamePosition furtherFwdRight(inPos.getRow() + (inDir * 2), inPos.getCol() + 2);
			if (isPosWithinBoard(furtherFwdRight)) {

				if (isPosEqualPiece(furtherFwdRight, PieceType::NONE)) {
					retPoss.emplace_back(furtherFwdRight);
				}
			}
		}

	}


}

void GameController::getValidBackPositions (GamePosition& inPos, int inDir, GameState& inGameState, std::vector<GamePosition>& retPoss) {
//	Debug::info("GameController::getValidBackPositions() entered..\n");
	getValidForwardPositions(inPos, (inDir * -1), inGameState, retPoss);
}

void GameController::getValidPositions (GamePosition& inPos, GameState& inGameState, std::vector<GamePosition>& retPoss) {

	//direction multiplier, anything to do with add/subtracting
	//of row gets multipled by this, which is based on if it's
	//red or white moving. This is so we can use the same methods
	//for validation, with calculations being done in the
	//'forward' direction for that player/colour.
	int dirMult = 0;
	if (inGameState == (int)RED_TURN) {
		dirMult = 1;
	} else {
		dirMult = -1;
	}

	getValidForwardPositions(inPos, dirMult, inGameState, retPoss);
	if (isPosEqualPiece(inPos, PieceType::RED_KING) || isPosEqualPiece(inPos, PieceType::WHITE_KING)) {
		getValidForwardPositions(inPos, -dirMult, inGameState, retPoss);
	}	
}

bool GameController::isMoveValid (GameMove& inMove, GameState& inGameState) {
//	Debug::info("GameController::isMoveValid() entered..\n");
	
	GameModel* gameModel = dynamic_cast<GameModel*>(model.get());

	//is the player moving their own piece (or a piece at all?)
	GameState gameState = gameModel->getGame()->getGameState();
	if (!isPosPlayers(gameState, inMove.getFrom())) {
		Debug::error("GameController::isMoveValid() player desn't own piece!\n");
		return false;		
	}


	//vector to store valid positions to check against
	//get all possible positions for the player could have
	//moved their piece to, and check 'to' position passed 
	//in is one of them.
	std::vector<GamePosition> validPositions;
	getValidPositions(inMove.getFrom(), inGameState, validPositions);

	bool isToPosValid = false;
//	Debug::info("GameController::isMoveValid() valid positions calucalated:\n");
	for (auto iter = validPositions.begin(); iter != validPositions.end(); iter++) {
		std::cout << "\trow/col " << iter->getRow() << "/" << iter->getCol() << "\n";
		if (*iter == inMove.getTo()) {
			isToPosValid = true;
			break;
		}
	}	
	if (!isToPosValid) {
		Debug::error("Sorry, the position is not valid!\n");
//		Debug::error("GameController::isMoveValid(): To pos wasn't in valid pos list\n");
		return false;
//		Debug::todo("Uncomment here! isToPosValid() call is always returning true at the moment..\n");
	}

	return true;
}

void GameController::swapPlayerTurns () {

	GameModel& gameModel = dynamic_cast<GameModel&>(*model);			
	if (gameModel.getGame()->getGameState() == RED_TURN) {
//		Debug::info("GameController::swapPlayerTurns(): It was RED, it will be player WHITESs turn..\n");
		gameModel.getGame()->setGameState(WHITE_TURN);
	} else if (gameModel.getGame()->getGameState() == WHITE_TURN) {
//		Debug::info("GameController::swapPlayerTurns(): It was WHITE, it will be REDs turn..\n");
		gameModel.getGame()->setGameState(RED_TURN);
	}


}


void GameController::checkPieceToKing (int inRow, int inCol) {
	GameModel* gameModel = dynamic_cast<GameModel*>(model.get());
	GameBoard& board = *gameModel->getGame()->getGameBoard();
	
	bool isKinging = false;
	int pieceType = board[inRow][inCol];
	
	if (pieceType == (int)PieceType::RED && inRow == (GAME_BOARD_SIZE - 1)) {
		board[inRow][inCol] = (int)PieceType::RED_KING;
		isKinging = true;
	} else if (pieceType == (int)PieceType::WHITE && inRow == 0) {
		board[inRow][inCol] = (int)PieceType::WHITE_KING;
		isKinging = true;
	}

	if (isKinging) {
		IO::note("Piece has become a King!\n");
	}

}



bool GameController::isGameEnd(Game& inGame) {

	auto redPiecesLeft = inGame.getRedPieces();
	auto whitePiecesLeft = inGame.getWhitePieces();

	if (redPiecesLeft == 0 || whitePiecesLeft == 0) {
		return true;
	}
	return false;


}


void GameController::doMove (GameMove* inMove) {
	//Debug::info("GameController::doMove() called..\n");
	
	//get whos turn it is from the model
	GameModel* gameModel = dynamic_cast<GameModel*>(model.get());
	GameState gameState = gameModel->getGame()->getGameState();
	
	if (!isMoveValid(*inMove, gameState)) {
		return;
	}

	GamePosition to = inMove->getTo();
	GamePosition from = inMove->getFrom();

	GameBoard& board = *gameModel->getGame()->getGameBoard();

	//move the piece at 'from' to the position of 'to'
	board[to.getRow()][to.getCol()] = board[from.getRow()][from.getCol()];

	checkPieceToKing(to.getRow(), to.getCol());


	if (isMoveCapturingPiece(gameState, *inMove)) {
		//get pos being captured
		int row = ((to.getRow() - from.getRow()) / 2) + from.getRow();
		int col = ((to.getCol() - from.getCol()) / 2) + from.getCol();
		GamePosition midPos(row, col);

		//determine if it was red or white being captured
		//increment the other plays score by one.
		if (isPosEqualPiece(midPos, PieceType::RED) || 
			isPosEqualPiece(midPos, PieceType::RED_KING)) {
			gameModel->getGame()->incWhitePlayerScore();
//			IO::note("White player score increased!\n");	 
		} else if (isPosEqualPiece(midPos, PieceType::WHITE) || 
			isPosEqualPiece(midPos, PieceType::WHITE_KING)) {
			gameModel->getGame()->incRedPlayerScore(); 
//			IO::note("Red player score increased!\n");	 
		} else {	
			Debug::error("isMoveCapturingPiece returned TRUE but cannot determine captured piece.\n");
		}
		
		//remove piece AFTER it's been determined who owned the piece being removed..	
		board[midPos.getRow()][midPos.getCol()] = (int)PieceType::NONE; 	
	}


	//Then set the old position to be empty/none. 
	//PieceType::NONE is enum class located in Game.h file.
	board[from.getRow()][from.getCol()] = (int)PieceType::NONE; 	

	swapPlayerTurns();

	//very crude end game for last minute
	if (isGameEnd(*gameModel->getGame())) {
		IO::print("WINNER!\n", GREEN);
		IO::print("Game has finished\n");
		std::unique_ptr<AppLoadMenu> appLoadMenu;
		appLoadMenu = std::make_unique<AppLoadMenu>(app);
		appLoadMenu->setMenuState(LOAD);	
		setNextAppCmd(std::move(appLoadMenu));
	}

}

void GameController::doLoadGame () {
	std::unique_ptr<AppLoadMenu> appLoadMenu;
	appLoadMenu = std::make_unique<AppLoadMenu>(app);
	appLoadMenu->setMenuState(LOAD);	
	setNextAppCmd(std::move(appLoadMenu));
}

void GameController::doSaveGame (Game* inGame) {
//	Debug::info("GameController::doSaveGame() called..\n");
	std::unique_ptr<Game> saveGame = std::make_unique<Game>(inGame);
	std::unique_ptr<AppSaveGame> appSaveGameCmd;
	appSaveGameCmd = std::make_unique<AppSaveGame>(app);
	appSaveGameCmd->setGame(std::move(saveGame));
	setNextAppCmd(std::move(appSaveGameCmd));

}
